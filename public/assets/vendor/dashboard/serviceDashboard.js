$(document).ready(function() {
    //Input DateRangePicker
    let initialDateService;
    let finalDateService;

    $(function() {

        $('input[name="dateFilterService"]').daterangepicker({
            autoUpdateInput: false,
            autoApply: true,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('input[name="dateFilterService"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
            initialDateService = picker.startDate.format('YYYY-MM-DD');
            finalDateService = picker.endDate.format('YYYY-MM-DD');
        });

        $('input[name="dateFilterService"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

    });

    //Filter Request In Service
    $('#btnFilterService').click(function() {
        let selectCustomerBranch = $('#selectCustomerBranch option:selected').val();
        let selectTechnicianService = $('#selectTechnicianService option:selected').val();
        let selectPlagueService = $('#selectPlagueService option:selected').val();
        let selectMountService = $('#selectMountService option:selected').val();
        let selectStatusService = $('#selectStatusService option:selected').val();

        initialDateService = initialDateService === undefined ? null : initialDateService;
        finalDateService = finalDateService === undefined ? null : finalDateService;

        window.location.href = `/dashboard?initialDateService=${initialDateService}&finalDateService=${finalDateService}&selectCustomerBranch=${selectCustomerBranch}&selectTechnicianService=${selectTechnicianService}&selectPlagueService=${selectPlagueService}&selectMountService=${selectMountService}&selectStatusService=${selectStatusService}`;
    });

    let selectBranch = document.getElementById("selectCustomerBranch");
    if(selectBranch != null)  {
        selectBranch.onchange = function(){
            let selectVal = document.getElementById("selectCustomerBranch").value
            window.location.href = `/dashboard?selectCustomerBranch=${selectVal}`;
        };
    }

    //Filter Request In Service
    $('#btnMain').click(function() {window.location.href = `/dashboard`;});

});
