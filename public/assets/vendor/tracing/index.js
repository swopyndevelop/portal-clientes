$(document).ready(function() {
    //Input DateRangePicker
    let initialDateApplication = null, finalDateApplication = null, initialDateAssignment = null, finalDateAssignment = null,
        folio = null, comments= null, technician = 0;

    $(function() {

        $('input[name="dateFilterApplication"]').daterangepicker({
            autoUpdateInput: false,
            autoApply: true,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('input[name="dateFilterApplication"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
            initialDateApplication = picker.startDate.format('YYYY-MM-DD');
            finalDateApplication = picker.endDate.format('YYYY-MM-DD');
        });

        $('input[name="dateFilterApplication"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

    });

    $(function() {

        $('input[name="dateFilterAssignment"]').daterangepicker({
            autoUpdateInput: false,
            autoApply: true,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('input[name="dateFilterAssignment"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
            initialDateAssignment = picker.startDate.format('YYYY-MM-DD');
            finalDateAssignment = picker.endDate.format('YYYY-MM-DD');
        });

        $('input[name="dateFilterAssignment"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

    });

    //Filter Request In Tracings
    $('#btnFilterTracing').click(function() {
        folio = $('#folioFilterTracing').val();
        comments = $('#folioFilterComment').val();
        technician = $('#selectTechnicianTracing option:selected').val();

        window.location.href = `/tracings?initialDateApplication=${initialDateApplication}&finalDateApplication=${finalDateApplication}&initialDateAssignment=${initialDateAssignment}&finalDateAssignment=${finalDateAssignment}&folio=${folio}&comments=${comments}&technician=${technician}`;
    });

    let selectCustomerBranchTracing = document.getElementById("selectCustomerBranchTracing")
    if (selectCustomerBranchTracing != null) {
        selectCustomerBranchTracing.onchange = function(){
            let selectVal = document.getElementById("selectCustomerBranchTracing").value
            window.location.href = `/tracings?selectCustomerBranchTracing=${selectVal}`;
        }}

    $('#modalCreateTracing').on('show.bs.modal', function (event) {
        let customerBranch = $(event.relatedTarget).data().branch;
        let customerMain = $(event.relatedTarget).data().customer;
        if (customerBranch === null) customerMain = customerBranch
        searchBranch(customerMain)
    });

    function searchBranch(id) {

        $.ajax({
            type: 'GET',
            url: `tracings/customer/${id}/`,
            data: {
                _token: $("meta[name=csrf-token]").attr("content")
            },
            success: function (data) {
                if (data.code === 500){
                    window.alert(data.message);
                }
                else {
                    let select = $('#selectCustomerBranches');
                    select.append(`<option value="${data.customer.id}">${data.customer.name}</option>`)
                    document.getElementById("emailTracingOne").value = data.personalMail.email_tracing;
                    //window.alert(data.message);
                }

            }
        });
    }

    $('#formTracingByBranch').on('submit', function(event) {

        event.preventDefault();
        saveTracingForm();

    });

    function saveTracingForm() {

        let fd = new FormData(document.getElementById("formTracingByBranch"));

        $.ajaxSetup({
            headers: { "X-CSRF-Token": $('meta[name="csrf-token"]').attr("content") }
        });
        $.ajax({
            type: 'POST',
            url: 'tracings/store',
            data: fd,
            contentType: false,
            processData: false,
            success: function (data) {
                Swal.close();
                if (data.code === 500) window.alert(data.message);
                else {
                    if (data.code === 400) window.alert(data.message);
                    if (data.code === 401) window.alert(data.message);
                    if (data.code === 201) {
                        window.alert("Se agrego correctamente el Seguimiento")
                        location.reload();
                    }
                }
            }
        });
    }

});
