$(document).ready(function() {
    //Input DateRangePicker
    let initialDateService;
    let finalDateService;

    $(function() {

        $('input[name="dateFilterArea"]').daterangepicker({
            autoUpdateInput: false,
            autoApply: true,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('input[name="dateFilterArea"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
            initialDateService = picker.startDate.format('YYYY-MM-DD');
            finalDateService = picker.endDate.format('YYYY-MM-DD');
        });

        $('input[name="dateFilterArea"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

    });

    //Filter Request In Service
    $('#btnFilterArea').click(function() {
        let folio = $('#areaInspectionFilterArea').val();
        let customerArea = $('#selectCustomerArea option:selected').val();
        let addressCustomerArea = $('#selectCustomerAddressArea option:selected').val();

        initialDateService = initialDateService === undefined ? null : initialDateService;
        finalDateService = finalDateService === undefined ? null : finalDateService;

        window.location.href = `/area/inspections?initialDateService=${initialDateService}&finalDateService=${finalDateService}&folio=${folio}&customerArea=${customerArea}&addressCustomerArea=${addressCustomerArea}`;
    });
});
