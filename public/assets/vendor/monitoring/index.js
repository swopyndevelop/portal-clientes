$(document).ready(function() {
    //Input DateRangePicker
    let initialDateService;
    let finalDateService;

    $(function() {

        $('input[name="dateFilterMonitoring"]').daterangepicker({
            autoUpdateInput: false,
            autoApply: true,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('input[name="dateFilterMonitoring"]').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
            initialDateService = picker.startDate.format('YYYY-MM-DD');
            finalDateService = picker.endDate.format('YYYY-MM-DD');
        });

        $('input[name="dateFilterMonitoring"]').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
        });

    });

    //Filter Request In Service
    $('#btnFilterMonitoring').click(function() {
        let folio = $('#inspectionFilterMonitoring').val();
        let customerMonitoring = $('#selectCustomerMonitoring option:selected').val();
        let addressCustomerMonitoring = $('#selectCustomerAddressMonitoring option:selected').val();

        initialDateService = initialDateService === undefined ? null : initialDateService;
        finalDateService = finalDateService === undefined ? null : finalDateService;

        window.location.href = `/inspections?initialDateService=${initialDateService}&finalDateService=${finalDateService}&folio=${folio}&customerMonitoring=${customerMonitoring}&addressCustomerMonitoring=${addressCustomerMonitoring}`;
    });
});
