<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TracingMail extends Mailable
{
    use Queueable, SerializesModels;

    private $tracing;
    private $message;

    /**
     * Tracing Mail constructor.
     * @param $tracing
     * @param $mesagge
     */

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($tracing, $message)
    {
        $this->tracing = $tracing;
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->subject( 'Seguimiento : ' . $this->tracing->id_service_order)
            ->markdown('emails.viewTracing')
            ->with(['message' => $this->message]);
    }
}
