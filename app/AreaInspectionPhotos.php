<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AreaInspectionPhotos
 * @package App
 * @property int $id
 * @property int $id_area_inspection
 * @property string $photo
 * @mixin Eloquent
 */
class AreaInspectionPhotos extends Model
{
    /**
     * @var string
     */
    protected $table = "area_inspections_photos";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_area_inspection',
        'photo'
    ];
}
