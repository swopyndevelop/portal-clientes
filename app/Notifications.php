<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Notifications
 * @package App
 * @package App
 * @property int $id
 * @property string $title
 * @property string $message
 * @property string $action_url
 * @property boolean $is_read
 * @property string|null $permission_read
 * @property int $id_profile_job_center
 * @property int $id_company
 * @mixin Eloquent
 */
class Notifications extends Model
{
    protected $table = 'notifications';

    protected $fillable = [
        'title',
        'message',
        'action_url',
        'is_read',
        'permission_read',
        'id_profile_job_center',
        'id_company'
    ];
}
