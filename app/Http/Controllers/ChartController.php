<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ChartController extends Controller
{
    public function data(Request $request)
    {

        // Get request
        $initialDate = $request->get('initial_date');
        $finalDate = $request->get('final_date');

        // Get services by customer and date
        $services = $this->getServicesByCustomerAndDate($initialDate, $finalDate);

        // Get plagues with grade infestation
        $plaguesWithGrade = $this->getPlaguesWithGradeInfestation($services);

        return response()->json([
            'datasets' => $plaguesWithGrade
        ]);

    }

    private function getServicesByCustomerAndDate($initialDate, $finalDate)
    {

        return DB::table('events as e')
            ->join('service_orders as so', 'e.id_service_order', 'so.id')
            ->join('quotations as q', 'so.id_quotation', 'q.id')
            ->join('customers as c', 'q.id_customer', 'c.id')
            ->join('place_inspections as pi', 'pi.id_service_order', 'so.id')
            ->select('so.id as id_order', 'pi.id as id_place_inspection', 'e.initial_date as date')
            ->whereDate('e.initial_date', '>=', $initialDate)
            ->whereDate('e.initial_date', '<=', $finalDate)
            ->where('c.user_id', auth()->user()->id)
            ->get();
    }

    private function getPlaguesWithGradeInfestation($services)
    {

        foreach ($services as $service) {

            $services->map(function ($service) {
                $plagues = $this->getPlaguesByService($service->id_place_inspection);
                $service->plagues = $plagues;
            });
            
        }

        return $services;
    }

    private function getPlaguesByService($id)
    {

        return DB::table('place_inspection_plague_type as pipt')
            ->join('plague_types as p', 'pipt.plague_type_id', 'p.id')
            ->join('infestation_degrees as ind', 'pipt.id_infestation_degree', 'ind.id')
            ->select('p.name as plague', 'ind.name as grade')
            ->where('place_inspection_id', $id)
            ->orderBy('plague', 'asc')
            ->get();
    }
}
