<?php

namespace App\Http\Controllers\ReportsPDF;

use App\Area_tree;
use App\Common\CommonImage;
use App\customer_branche;
use App\Http\Controllers\Business\CommonCompany;
use App\profile_job_center;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Illuminate\Support\Facades\DB;

class ServiceCertificate
{
    public static function build($id)
    {
        $order = DB::table('events as e')
            ->join('employees as em','e.id_employee','em.id')
            ->join('profile_job_centers as pjc','e.id_job_center','pjc.id')
            ->join('service_orders as so','e.id_service_order','so.id')
            ->join('payment_methods as pm','so.id_payment_method','pm.id')
            ->join('payment_ways as pw','so.id_payment_way','pw.id')
            ->join('users as u','so.user_id','u.id')
            ->join('statuses as st','so.id_status','st.id')
            ->join('quotations as q','so.id_quotation','q.id')
            ->join('establishment_types as et','q.establishment_id','et.id')
            ->join('customers as c','q.id_customer','c.id')
            ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
            ->join('plague_types as pt', 'q.id_plague_jer', 'pt.id')
            ->join('discount_quotation as qd', 'q.id', 'qd.quotation_id')
            ->join('discounts as d', 'qd.discount_id', 'd.id')
            ->join('extras as ex', 'q.id_extra', 'ex.id')
            ->join('price_lists as pl', 'q.id_price_list', 'pl.id')
            ->select('so.id as order', 'so.id_service_order', 'so.created_at as date', 'e.initial_hour',
                'e.initial_date', 'em.name', 'em.file_route_firm', 'c.name as cliente','c.establishment_name as empresa',
                'c.cellphone','cd.address','c.municipality', 'q.id_plague_jer','q.total','pt.plague_key',
                'u.name as agente','st.name as status','cd.address_number','cd.state','cd.email','e.id_status',
                'e.id as event','c.colony','q.id as quotation','e.final_hour','e.final_date','so.address as a_rfc',
                'so.email as e_rfc','so.bussiness_name', 'so.observations','cd.billing','d.id as discount',
                'd.percentage','ex.id as extra','ex.amount','q.construction_measure','q.garden_measure','q.price',
                'q.establishment_id as e_id','pm.id as pm_id','pw.id as pw_id','pm.name as metodo','pw.name as tipo',
                'so.id_job_center', 'cd.email', 'q.companie as compania','e.start_event', 'e.final_event', 'pl.show_price',
                'et.name as establecimiento', 'so.customer_branch_id', 'so.total as total_order', 'cd.customer_id',
                'pl.portada as pdf_mip', 'pjc.sanitary_license', 'pl.days_expiration_certificate', 'c.show_price as show_price_customer',
                'so.date_expiration_certificate', 'so.area_node_id', 'c.days_expiration_certificate as days_expiration_certificate_customer',
                'pl.is_disinfection')
            ->where('so.id',$id)
            ->first();

        $imagen = DB::table('companies')
            ->select('pdf_logo', 'pdf_sello', 'phone', 'licence', 'facebook', 'warnings_service',
                'contract_service', 'pdf_sanitary_license', 'rfc','bussines_name', 'health_manager', 'email')
            ->where('id', $order->compania)
            ->first();
        $jobCenterProfile = profile_job_center::where('id', $order->id_job_center)->first();
        $sanitary_license_qr = env('AWS_STORAGE_PUBLIC') . $jobCenterProfile->sanitary_license;
        $pdf_logo = CommonImage::getTemporaryUrl($imagen->pdf_logo, 5);
        $pdf_sello = CommonImage::getTemporaryUrl($imagen->pdf_sello, 5);

        if ($order->sanitary_license != null) {
            $qrcode = base64_encode(QrCode::format('svg')->size(90)->errorCorrection('H')->generate($sanitary_license_qr));
        } else $qrcode = null;

        //Condiciones del Lugar
        $ins = DB::table('order_cleaning_place_condition as co')
            ->join('place_conditions as pc','co.place_condition_id','pc.id')
            ->join('order_cleanings as o','co.order_cleaning_id','o.id')
            ->select('co.place_condition_id','o.name as order')->where('pc.id_service_order',$id)->get();

        //Grado de Infestacion de Plagas
        $plaga = DB::table('place_inspection_plague_type as pt')
            ->join('place_inspections as pi','pt.place_inspection_id','pi.id')
            ->join('plague_types as pl','pt.plague_type_id','pl.id')
            ->join('infestation_degrees as ie','pt.id_infestation_degree','ie.id')
            ->select('pl.name as plaga','ie.name as infestacion')->where('pi.id_service_order',$id)->get();

        $ani = DB::table('place_inspections')->where('id_service_order',$id)->first();

        //Control de Plagas
        $control = DB::table('plague_controls_products as pc')
            ->join('plague_controls as pl','pc.plague_control_id','pl.id')
            ->join('service_orders as so','pl.id_service_order','so.id')
            ->join('products as p','pc.id_product','p.id')
            ->join('product_units as pu', 'p.id_unit', 'pu.id')
            ->select('pl.id_service_order','p.name as product','pc.dose','pc.quantity','p.id as id_product','pl.control_areas',
                'p.active_ingredient','pc.quantity as cantidad', 'pu.name as type_unit')
            ->where('pl.id_service_order', $id)
            ->get();

        $area_c = DB::table('plague_controls as pc')
            ->select('id_service_order','control_areas','commentary')
            ->where('id_service_order',$id)->get();

        $area_cd = DB::table('plague_controls as pc')
            ->join('plague_controls_application_methods as pca','pc.id','pca.plague_control_id')
            ->join('application_methods as am','pca.id_application_method','am.id')
            ->select('id_service_order','control_areas','am.name as apli')
            ->where('id_service_order',$id)->get();


        $indi = DB::table('place_conditions')->where('id_service_order',$id)->first();

        $service_firms = DB::table('service_firms as sf')
            ->select('sf.file_route', 'sf.other_name')
            ->where('sf.id_service_order',$id)->first();

        $firmUrl = "";
        $firmUrlTechnician = "";
        if ($service_firms) {
            $firmUrl = CommonImage::getTemporaryUrl($service_firms->file_route, 5);
        }
        if ($order->file_route_firm != null) $firmUrlTechnician = CommonImage::getTemporaryUrlPublic($order->file_route_firm, 5);

        //Ajuste de datos si es cotización personalizada
        if ($order->customer_branch_id != null) {
            $customerBranch = customer_branche::find($order->customer_branch_id);
            $order->total = $order->total_order;
            $order->empresa = $customerBranch->name;
            $order->address = $customerBranch->address;
            $order->address_number = $customerBranch->address_number;
            $order->colony = $customerBranch->colony;
            $order->municipality = $customerBranch->municipality;
            $order->state = $customerBranch->state;
        }

        $symbol_country = CommonCompany::getSymbolByCountryWithCompanyId($order->compania);
        $jobCenterProfile = profile_job_center::where('id', $order->id_job_center)->first();
        $addressProfile = DB::table('address_job_centers')
            ->where('profile_job_centers_id', $jobCenterProfile->profile_job_centers_id)->first();

        // Validación para ordenes de tipo qr área con certificado.
        $textNode = "";
        if ($order->area_node_id != null) {
            $areaTree = Area_tree::find($order->area_node_id);
            $textNode = $areaTree->text;
        }
        $order->textNode = $textNode;

        return [
            'order' => $order,
            'jobCenterProfile' => $jobCenterProfile,
            'addressProfile' => $addressProfile,
            'ins' => $ins,
            'plaga' => $plaga,
            'control' => $control,
            'f' => $service_firms,
            'area_c' => $area_c,
            'ani' => $ani,
            'indi' => $indi,
            'area_cd' => $area_cd,
            'imagen'=> $imagen,
            'qrcode' => $qrcode,
            'symbol_country' => $symbol_country,
            'firmUrl' => $firmUrl,
            'firmUrlTechnician' => $firmUrlTechnician,
            'pdf_logo' => $pdf_logo,
            'pdf_sello' => $pdf_sello
        ];

    }
}
