<?php

namespace App\Http\Controllers;

use App\customer;
use App\employee;
use App\event;
use App\Mail\TracingMail;
use App\Notifications;
use App\personal_mail;
use App\quotation;
use App\service_order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class TracingController extends Controller
{
    public function show(Request $request)
    {
        $customer = DB::table('customers')->where('user_id', Auth::user()->id)->first();
        $selectCustomerBranch = $request->get('selectCustomerBranchTracing');

        if ($selectCustomerBranch != null) {
            $customerBranch = customer::find($selectCustomerBranch);
            $customer = $customerBranch;
        }

        // Filters Request
        $initialDateApplication = $request->get('initialDateApplication');
        if (!$request->exists('initialDateApplication')) $initialDateApplication = "null";
        $finalDateApplication = $request->get('finalDateApplication');
        if (!$request->exists('finalDateApplication')) $finalDateApplication = "null";
        $initialDateAssignment = $request->get('initialDateAssignment');
        if (!$request->exists('initialDateAssignment')) $initialDateAssignment = "null";
        $finalDateAssignment = $request->get('finalDateAssignment');
        if (!$request->exists('finalDateAssignment')) $finalDateAssignment = "null";
        $folio = $request->get('folio');
        $comments = $request->get('comments');
        $technician = $request->get('technician');

        $tracings = DB::table('service_orders as so')
            ->join('quotations as q', 'so.id_quotation', 'q.id')
            ->join('events as ev', 'so.id', 'ev.id_service_order')
            ->join('employees as emp', 'ev.id_employee', 'emp.id')
            ->where('so.tracing', 1)
            ->where('q.id_customer', $customer->id)
            ->select('so.id_service_order', 'so.created_at', 'ev.initial_date', 'ev.initial_hour', 'emp.name',
                'ev.id_status', 'so.tracing_comments', 'so.evidence_tracings');

        if ($folio != null) $tracings->where('so.id_service_order','like','%'. $folio . '%');
        if ($initialDateApplication != "null" && $finalDateApplication != "null") {
            $tracings->whereDate('so.created_at', '>=', $initialDateApplication)
                ->whereDate('so.created_at', '<=', $finalDateApplication);
        }
        if ($initialDateAssignment != "null" && $finalDateAssignment != "null") {
            $tracings->whereDate('ev.initial_date', '>=', $initialDateAssignment)
                ->whereDate('ev.initial_date', '<=', $finalDateAssignment);
        }
        if ($comments != null) $tracings->where('so.tracing_comments','like','%'. $comments . '%');
        if ($technician != 0) $tracings->where('emp.id', $technician);

        $employees = employee::where('profile_job_center_id', $customer->id_profile_job_center)->get();
        $customerSelectBranches = DB::table('customers')->where('user_id', Auth::user()->id)->first();
        $customerBranches = DB::table('customers')
            ->where('is_main', 0)
            ->where('customer_main_id', $customerSelectBranches->id)
            ->get();

        $tracingsFilter = $tracings->orderByDesc('so.created_at')->paginate(10)->appends([
            'initialDateApplication' => $initialDateApplication,
            'finalDateApplication' => $finalDateApplication,
            'folio' => $folio,
            'comments' => $comments,
        ]);

        return view('dashboard.tracing')->with([
            'customer' => $customer,
            'customerBranches' => $customerBranches,
            'selectCustomerBranch' => $selectCustomerBranch,
            'tracings' => $tracingsFilter,
            'employees' => $employees
        ]);
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $customer = DB::table('customers')->where('user_id', Auth::user()->id)->first();
            $customerBranchRequest = $request->get('selectCustomerBranches');
            $emailOne = $request->get('emailTracingOne');
            $emailTwo = $request->get('emailTracingTwo');

            if ($customerBranchRequest !== null) {
                $customerBranch = customer::find($customerBranchRequest);
                $customer = $customerBranch;
            }

            $lastOrder = DB::table('service_orders as so')
                ->join('quotations as q', 'so.id_quotation', 'q.id')
                ->join('events as ev', 'so.id', 'ev.id_service_order')
                ->join('employees as em', 'ev.id_employee', 'em.id')
                ->where('ev.id_status', 4)
                ->where('so.warranty', 0)
                ->where('so.tracing', 0)
                ->where('so.reinforcement', 0)
                ->where('q.id_customer', $customer->id)
                ->select('so.id', 'so.created_at', 'so.user_id', 'so.companie','em.name')
                ->latest()
                ->first();

            $lastTracing = DB::table('service_orders as so')
                ->join('quotations as q', 'so.id_quotation', 'q.id')
                ->join('events as ev', 'so.id', 'ev.id_service_order')
                ->join('employees as em', 'ev.id_employee', 'em.id')
                ->where('ev.id_status', null)
                ->where('so.tracing', 1)
                ->where('q.id_customer', $customer->id)
                ->select('so.id', 'so.created_at', 'so.user_id', 'so.companie', 'em.name')
                ->latest()
                ->first();

            if ($lastTracing) {
                return \response()->json([
                    'code' => 400,
                    'message' => 'Ya cuentas con un seguimiento abierto. Espera su finalización o solicita la cancelación.'
                ]);
                //return back()->with(['error' => 'Ya cuentas con un seguimiento abierto. Espera su finalización o solicita la cancelación.']);
            }
            if ($lastOrder == null) {
                return \response()->json([
                    'code' => 401,
                    'message' => 'Aún no cuentas con servicios finalizados.'
                ]);
                //return back()->with(['error' => 'Aún no cuentas con servicios finalizados.']);
            }

            $order = service_order::find($lastOrder->id);
            $order->tracing = $order->tracing++;
            $order->save();

            $quote = quotation::find($order->id_quotation);

            $folio = explode('-', $quote->id_quotation);
            $lastFolio = $folio[1];

            $folio = $this->getFolioTracing($order->id_quotation, $lastFolio, $lastOrder->companie);

            $eventOld = event::where('id_service_order', $lastOrder->id)->first();

            $tracing = new service_order;
            $tracing->id_service_order = $folio;
            $tracing->id_quotation = $order->id_quotation;
            $tracing->id_payment_method = $order->id_payment_method;
            $tracing->id_payment_way = $order->id_payment_way;
            $tracing->bussiness_name = $order->bussiness_name;
            $tracing->address = $order->address;
            $tracing->email = $order->email;
            $tracing->observations = $order->observations;
            $tracing->total = 0;
            $tracing->user_id = $lastOrder->user_id;
            $tracing->id_job_center = $order->id_job_center;
            $tracing->companie = $lastOrder->companie;
            $tracing->id_status = 10;
            $tracing->tracing = 1;
            $tracing->warranty = 0;
            $tracing->reinforcement = 0;
            $tracing->tracing_comments = $request->get('tracingComments');
            $tracing->save();

            if($tracing->save()){
                if($request->evidence != null) {
                    $filename = $request->file('evidence')->store(
                        'customer_tracings', 'ftp'
                    );
                    $tracingEvidence = service_order::find($tracing->id);
                    $tracingEvidence->evidence_tracings = $filename;
                    $tracingEvidence->save();
                }
            }

            $event = new event;
            $event->title = 'Seguimiento';
            $event->id_service_order = $tracing->id;
            $event->id_employee = $eventOld->id_employee;
            $event->id_job_center = $tracing->id_job_center;
            $event->companie = $lastOrder->companie;
            $event->save();

            $messageNotification = 'Se solicito el seguimiento a: ' . $lastOrder->name . ' el Día: ' . $tracing->created_at . ' con los siguientes comentarios: ' . $tracing->tracing_comments;

            $notification = new Notifications;
            $notification->title = $event->title . ' a ' . $customer->name . ' => ' . $folio;
            $notification->message = $messageNotification;
            $notification->is_read = 0;
            $notification->id_profile_job_center = $event->id_job_center;
            $notification->id_company = $event->companie;
            $notification->save();

            $result = filter_var($emailOne, FILTER_VALIDATE_EMAIL);
            if ($result != false) {
                $fromTracing = 'Portal Clientes => ' . $customer->name;
                config(['mail.from.name' => $fromTracing]);
                Mail::to($emailOne)->send(new TracingMail($tracing, $messageNotification));
            }

            $resultTwo = filter_var($emailTwo, FILTER_VALIDATE_EMAIL);
            if ($resultTwo != false) {
                $fromTracing = 'Portal Clientes => ' . $customer->name;
                config(['mail.from.name' => $fromTracing]);
                Mail::to($emailTwo)->send(new TracingMail($tracing, $messageNotification));
            }

            DB::commit();

            return \response()->json([
                'code' => 201,
                'message' => 'Data'
            ]);

        }catch (\Exception $exception) {
            DB::rollBack();
            return \response()->json([
                'code' => 500,
                'message' => $exception->getLine()
            ]);
        }
    }

    public function getCustomer($id) {
        try {
            $customer =  customer::find($id);
            $personalMail = personal_mail::where('profile_job_center_id', $customer->id_profile_job_center)->first();
            return \response()->json([
                'code' => 201,
                'customer' => $customer,
                'personalMail' => $personalMail,
                'message' => 'Se realizó correctamente la acción'
            ]);

        }catch (\Exception $exception) {
            return \response()->json([
                'code' => 500,
                'message' => 'Algo salió mal, intentalo de nuevo'
            ]);
        }
    }

    private function getFolioTracing($quotationId, $lastFolio, $company): string
    {
        $tracing = service_order::where('companie', $company)
            ->where('tracing', 1)
            ->where('id_quotation', $quotationId)
            ->latest()->first();

        if ($tracing) {
            $folio = explode('-', $tracing->id_service_order);
            return 'S-' . $lastFolio . '-' . ++$folio[2];
        } else return 'S-' . $lastFolio . '-1';
    }
}
