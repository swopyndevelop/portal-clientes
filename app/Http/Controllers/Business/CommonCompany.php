<?php

namespace App\Http\Controllers\Business;

use App\companie;
use App\Country;
use App\employee;
use Illuminate\Support\Facades\Auth;

class CommonCompany
{
    public static function getCompanyById($companyId)
    {
        return companie::find($companyId);
    }

    public static function getProfileJobCenterId()
    {
        return employee::where('employee_id', Auth::user()->id)->first()->profile_job_center_id;
    }

    public static function getSymbolByCountry()
    {
        $company = companie::find(\Auth::user()->companie);
        $countryCode = Country::find($company->id_code_country);
        return $countryCode->symbol_country;
    }

    public static function getSymbolByCountryPublic($companyId)
    {
        $company = companie::find($companyId);
        $countryCode = Country::find($company->id_code_country);
        return $countryCode->symbol_country;
    }

    public static function getCodeByCountry()
    {
        $company = companie::find(\Auth::user()->companie);
        $countryCode = Country::find($company->id_code_country);
        return $countryCode->code_country;
    }

    public static function getSymbolByCountryWithCompanyId($companyId)
    {
        $company = companie::find($companyId);
        $countryCode = Country::find($company->id_code_country);
        return $countryCode->symbol_country;
    }
}
