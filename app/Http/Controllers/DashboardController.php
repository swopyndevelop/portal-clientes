<?php

namespace App\Http\Controllers;

use App\Area;
use App\Area_tree;
use App\AreaInspectionPhotos;
use App\Common\CommonImage;
use App\customer;
use App\employee;
use App\Http\Controllers\Business\CommonCompany;
use App\Http\Controllers\ReportsPDF\ServiceCertificate;
use App\Http\Controllers\ReportsPDF\ServiceOrder;
use App\Http\Controllers\Security\SecurityController;
use App\profile_job_center;
use App\StationInspection;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use iio\libmergepdf\Driver\TcpdiDriver;
use iio\libmergepdf\Merger;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use PDF;
use Response;
use PDFMerger;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {

        $initialDateService = $request->get('initialDateService');
        $finalDateService = $request->get('finalDateService');
        $selectTechnicianService = $request->get('selectTechnicianService');
        $selectPlagueService = $request->get('selectPlagueService');
        $selectMountService = $request->get('selectMountService');
        $selectStatusService = $request->get('selectStatusService');
        $selectCustomerBranch = $request->get('selectCustomerBranch');

        if ($initialDateService == "null") $initialDateService = null;
        if ($finalDateService == "null") $finalDateService = null;
        if ($selectStatusService == '0') $selectStatusService = null;
        if ($selectMountService == '0') $selectMountService = null;
        if ($selectPlagueService == '0') $selectPlagueService = null;
        if ($selectTechnicianService == '0') $selectTechnicianService = null;

        $customer = DB::table('customers')->where('user_id', auth()->user()->id)->first();
        if ($selectCustomerBranch != null) {
            $customerBranch = customer::find($selectCustomerBranch);
            $customer = $customerBranch;
        }
        if ($initialDateService == null) {
            if ($selectCustomerBranch == null) {
                $orders = DB::table('events as e')->join('employees as em','e.id_employee','em.id')
                    ->join('profile_job_centers as pjc','e.id_job_center','pjc.id')
                    ->join('service_orders as so','e.id_service_order','so.id')
                    ->join('payment_methods as pm','so.id_payment_method','pm.id')
                    ->join('payment_ways as pw','so.id_payment_way','pw.id')
                    ->join('users as u','so.user_id','u.id')
                    ->join('statuses as st','so.id_status','st.id')
                    ->join('quotations as q','so.id_quotation','q.id')
                    ->join('establishment_types as et','q.establishment_id','et.id')
                    ->join('customers as c','q.id_customer','c.id')
                    ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
                    ->join('plague_types as pt', 'q.id_plague_jer', 'pt.id')
                    ->join('discount_quotation as qd', 'q.id', 'qd.quotation_id')
                    ->join('discounts as d', 'qd.discount_id', 'd.id')
                    ->join('extras as ex', 'q.id_extra', 'ex.id')
                    ->join('price_lists as pl', 'q.id_price_list', 'pl.id')
                    ->select('so.id as order','so.id_service_order','so.created_at as date','e.initial_hour','e.initial_date','em.name',
                        'c.name as cliente','c.establishment_name as empresa','c.cellphone','cd.address','c.municipality', 'q.id_plague_jer','so.total',
                        'pt.plague_key','u.name as agente','st.name as status','cd.address_number','cd.state','cd.email','e.id_status',
                        'c.colony', 'so.warranty', 'so.reinforcement', 'so.tracing', 'q.establishment_id', 'so.whatsapp', 'q.id as id_quotation',
                        'so.confirmed', 'so.reminder', 'so.id_status as status_order', 'so.companie', 'e.id as id_event', 'pl.key','pl.id as lista',
                        'q.construction_measure', 'so.customer_branch_id','e.service_type', 'pl.show_price', 'c.show_price as show_price_customer')
                    ->where(function ($query) use ($customer) {
                        $query->where('c.customer_main_id', $customer->id)
                            ->orWhere('c.id', $customer->id);
                    });

                if ($selectPlagueService != null) $orders = $orders->where('pl.id', $selectPlagueService);
                if ($selectTechnicianService != null) $orders = $orders->where('em.employee_id', $selectTechnicianService);
                if ($selectStatusService != null) $orders = $orders->where('e.id_status', $selectStatusService);
                if ($selectStatusService == 6) $orders->where('e.service_type', 1);
                if ($selectStatusService == 7) $orders->where('e.service_type', 3);
                if ($selectStatusService == 8) $orders->where('e.service_type', 2);
                if ($selectStatusService == 9) $orders->where('e.service_type', 4);

                $orders = $orders->orderBy('so.id', 'desc')->paginate(10)->appends([
                    //'selectStatusService' => $selectStatusService
                    //'initialDateService' => $initialDateService,
                ]);
            }
            else {
                $orders = DB::table('events as e')->join('employees as em','e.id_employee','em.id')
                    ->join('profile_job_centers as pjc','e.id_job_center','pjc.id')
                    ->join('service_orders as so','e.id_service_order','so.id')
                    ->join('payment_methods as pm','so.id_payment_method','pm.id')
                    ->join('payment_ways as pw','so.id_payment_way','pw.id')
                    ->join('users as u','so.user_id','u.id')
                    ->join('statuses as st','so.id_status','st.id')
                    ->join('quotations as q','so.id_quotation','q.id')
                    ->join('establishment_types as et','q.establishment_id','et.id')
                    ->join('customers as c','q.id_customer','c.id')
                    ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
                    ->join('plague_types as pt', 'q.id_plague_jer', 'pt.id')
                    ->join('discount_quotation as qd', 'q.id', 'qd.quotation_id')
                    ->join('discounts as d', 'qd.discount_id', 'd.id')
                    ->join('extras as ex', 'q.id_extra', 'ex.id')
                    ->join('price_lists as pl', 'q.id_price_list', 'pl.id')
                    ->select('so.id as order','so.id_service_order','so.created_at as date','e.initial_hour','e.initial_date','em.name',
                        'c.name as cliente','c.establishment_name as empresa','c.cellphone','cd.address','c.municipality', 'q.id_plague_jer','so.total',
                        'pt.plague_key','u.name as agente','st.name as status','cd.address_number','cd.state','cd.email','e.id_status',
                        'c.colony', 'so.warranty', 'so.reinforcement', 'so.tracing', 'q.establishment_id', 'so.whatsapp', 'q.id as id_quotation',
                        'so.confirmed', 'so.reminder', 'so.id_status as status_order', 'so.companie', 'e.id as id_event', 'pl.key','pl.id as lista',
                        'q.construction_measure', 'so.customer_branch_id','e.service_type', 'pl.show_price', 'c.show_price as show_price_customer')
                    ->where('c.id', $customer->id);
                   // ->orderBy('so.id','DESC')
                    //->paginate(10);

                if ($selectPlagueService != null) $orders = $orders->where('pl.id', $selectPlagueService);
                if ($selectTechnicianService != null) $orders->where('em.employee_id', $selectTechnicianService);
                if ($selectStatusService != null) $orders = $orders->where('e.id_status', $selectStatusService);
                if ($selectStatusService == 6) $orders->where('e.service_type', 1);
                if ($selectStatusService == 7) $orders->where('e.service_type', 3);
                if ($selectStatusService == 8) $orders->where('e.service_type', 2);
                if ($selectStatusService == 9) $orders->where('e.service_type', 4);

                $orders = $orders->orderBy('so.id', 'desc')->paginate(10);

            }

            foreach ($orders as $o) {
                $orders->map(function($o){
                    if ($o->customer_branch_id != null) {
                        $branch = DB::table('customer_branches')->where('id', $o->customer_branch_id)->first();
                        $o->empresa = $branch->name;
                        $o->municipality = $branch->municipality;
                    }
                    if ($o->id_status == 1) {
                        $plagues = DB::table('plague_type_quotation as pq')
                            ->join('plague_types as pt', 'pq.plague_type_id', 'pt.id')
                            ->where('pq.quotation_id', $o->id_quotation)->get();

                        $plaguesText = "";
                        foreach ($plagues as $plague) {
                            $plaguesText = $plaguesText . $plague->name . ", ";
                        }
                        $pricesList = DB::table('price_lists')
                            ->where('company_id', $o->companie)
                            ->where('establishment_id', $o->establishment_id)
                            ->first();
                    }
                    if ($o->id_status == 3) {
                        $motive = DB::table('canceled_service_orders')->where('id_service_order', $o->order)->first();
                        $o->motive = $motive->reason . ", " . $motive->commentary;
                    }
                    if ($o->id_status != 3) {
                        $cashes = DB::table('cashes')->where('id_event', $o->id_event)->first();
                        $conditions = DB::table('place_conditions')->where('id_service_order', $o->order)->first();
                        $rating = DB::table('ratings_service_orders')->where('id_service_order', $o->order)->first();

                        if(!empty($cashes)){
                            if($cashes->payment == 1) $o->adeudo = 1;
                            else $o->adeudo = 2;
                            if ($cashes->id_payment_way == 1) $o->credito = 1;
                            else $o->credito = 2;
                        }
                        else {
                            $o->adeudo = null;
                            $o->credito = null;
                        }
                    }
                });
            }

        }
        else {
            //Filters Table
            $order = DB::table('events as e')->join('employees as em','e.id_employee','em.id')
                ->join('profile_job_centers as pjc','e.id_job_center','pjc.id')
                ->join('service_orders as so','e.id_service_order','so.id')
                ->join('payment_methods as pm','so.id_payment_method','pm.id')
                ->join('payment_ways as pw','so.id_payment_way','pw.id')
                ->join('users as u','so.user_id','u.id')
                ->join('statuses as st','so.id_status','st.id')
                ->join('quotations as q','so.id_quotation','q.id')
                ->join('establishment_types as et','q.establishment_id','et.id')
                ->join('customers as c','q.id_customer','c.id')
                ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
                ->join('plague_types as pt', 'q.id_plague_jer', 'pt.id')
                ->join('discount_quotation as qd', 'q.id', 'qd.quotation_id')
                ->join('discounts as d', 'qd.discount_id', 'd.id')
                ->join('extras as ex', 'q.id_extra', 'ex.id')
                ->join('price_lists as pl', 'q.id_price_list', 'pl.id')
                ->select('so.id as order','so.id_service_order','so.user_id','so.created_at as date','e.initial_hour','e.initial_date','em.name',
                    'c.name as cliente','c.establishment_name as empresa','c.cellphone','cd.address','c.municipality', 'q.id_plague_jer','so.total',
                    'pt.plague_key','u.name as agente','st.name as status','cd.address_number','cd.state','cd.email','e.id_status',
                    'c.colony', 'so.warranty', 'so.reinforcement', 'so.tracing', 'q.establishment_id', 'so.whatsapp', 'q.id as id_quotation',
                    'so.confirmed', 'so.reminder', 'so.id_status as status_order', 'so.companie', 'e.id as id_event', 'pl.key','pl.id as lista',
                    'q.construction_measure', 'so.customer_branch_id' ,'e.service_type', 'pl.show_price', 'c.show_price as show_price_customer');

            if ($selectCustomerBranch == null) {
               $order = $order->where('c.is_main',0)
                    ->where('c.customer_main_id', $customer->id);
            }

            else $order = $order->where('c.id', $customer->id);

            if ($initialDateService != null && $finalDateService != null) {
                $order = $order->whereDate('e.initial_date', '>=', $initialDateService)
                    ->whereDate('e.initial_date', '<=', $finalDateService);
            }

            if ($selectTechnicianService != 0) $order = $order->where('em.employee_id', $selectTechnicianService);
            if ($selectPlagueService != 0) $order = $order->where('pl.id', $selectPlagueService);
            if ($selectMountService != 0) {
                if ($selectMountService == 3) $order = $order->join('cashes as ch', 'ch.id_service_order', 'so.id')->where('ch.id_payment_way', 1);
                else if ($selectMountService == 4) $order = $order->join('place_conditions as pc', 'pc.id_service_order', 'so.id')->where('indications', 2);
                else $order->join('cashes as ch', 'ch.id_service_order', 'so.id')->where('ch.payment', $selectMountService);
            }
            if ($selectStatusService > 0 && $selectStatusService < 6) $order = $order->where('e.id_status', $selectStatusService);
            if ($selectStatusService == 6) $order = $order->where('e.service_type', 1);
            if ($selectStatusService == 7) $order = $order->where('e.service_type', 3);
            if ($selectStatusService == 8) $order = $order->where('e.service_type', 2);
            if ($selectStatusService == 9) $order = $order->where('e.service_type', 4);

            $orders = $order->orderby('so.id', 'DESC')->paginate(10)->appends([
                'initialDateService' => $request->initialDateService,
                'finalDateService' => $request->finalDateService,
                '$selectCustomerBranch' => $request->selectCustomerBranch,
                'selectTechnicianService' => $request->selectTechnicianService,
                'selectPlagueService' => $request->selectPlagueService,
                //'selectMountService' => $request->selectMountService,
                'selectStatusService' => $request->selectStatusService,
            ]);

            if(count($orders) === 0){
                $order = DB::table('events as e')->join('employees as em','e.id_employee','em.id')
                    ->join('profile_job_centers as pjc','e.id_job_center','pjc.id')
                    ->join('service_orders as so','e.id_service_order','so.id')
                    ->join('payment_methods as pm','so.id_payment_method','pm.id')
                    ->join('payment_ways as pw','so.id_payment_way','pw.id')
                    ->join('users as u','so.user_id','u.id')
                    ->join('statuses as st','so.id_status','st.id')
                    ->join('quotations as q','so.id_quotation','q.id')
                    ->join('establishment_types as et','q.establishment_id','et.id')
                    ->join('customers as c','q.id_customer','c.id')
                    ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
                    ->join('plague_types as pt', 'q.id_plague_jer', 'pt.id')
                    ->join('discount_quotation as qd', 'q.id', 'qd.quotation_id')
                    ->join('discounts as d', 'qd.discount_id', 'd.id')
                    ->join('extras as ex', 'q.id_extra', 'ex.id')
                    ->join('price_lists as pl', 'q.id_price_list', 'pl.id')
                    ->select('so.id as order','so.id_service_order','so.user_id','so.created_at as date','e.initial_hour','e.initial_date','em.name',
                        'c.name as cliente','c.establishment_name as empresa','c.cellphone','cd.address','c.municipality', 'q.id_plague_jer','so.total',
                        'pt.plague_key','u.name as agente','st.name as status','cd.address_number','cd.state','cd.email','e.id_status',
                        'c.colony', 'so.warranty', 'so.reinforcement', 'so.tracing', 'q.establishment_id', 'so.whatsapp', 'q.id as id_quotation',
                        'so.confirmed', 'so.reminder', 'so.id_status as status_order', 'so.companie', 'e.id as id_event', 'pl.key','pl.id as lista',
                        'q.construction_measure', 'so.customer_branch_id' ,'e.service_type', 'pl.show_price', 'c.show_price as show_price_customer');

                if ($selectCustomerBranch == null) $order = $order->where('c.id', $customer->id);

                if ($initialDateService != "null" && $finalDateService != "null") {
                    $order = $order->whereDate('e.initial_date', '>=', $initialDateService)
                        ->whereDate('e.initial_date', '<=', $finalDateService);
                }

                if ($selectTechnicianService != 0) $order = $order->where('em.employee_id', $selectTechnicianService);
                if ($selectPlagueService != 0) $order = $order->where('pl.id', $selectPlagueService);
                if ($selectMountService != 0) {
                    if ($selectMountService == 3) $order = $order->join('cashes as ch', 'ch.id_service_order', 'so.id')->where('ch.id_payment_way', 1);
                    else if ($selectMountService == 4) $order = $order->join('place_conditions as pc', 'pc.id_service_order', 'so.id')->where('indications', 2);
                    else $order->join('cashes as ch', 'ch.id_service_order', 'so.id')->where('ch.payment', $selectMountService);
                }
                if ($selectStatusService > 0 && $selectStatusService < 6) $order = $order->where('e.id_status', $selectStatusService);
                if ($selectStatusService == 6) $order = $order->where('e.service_type', 1);
                if ($selectStatusService == 7) $order = $order->where('e.service_type', 3);
                if ($selectStatusService == 8) $order = $order->where('e.service_type', 2);
                if ($selectStatusService == 9) $order = $order->where('e.service_type', 4);

                $orders = $order->orderby('so.id', 'DESC')->paginate(10)->appends([
                    'initialDateService' => $request->initialDateService,
                    'finalDateService' => $request->finalDateService,
                    //'selectBranchService' => $request->selectBranchService,
                    'selectTechnicianService' => $request->selectTechnicianService,
                    'selectPlagueService' => $request->selectPlagueService,
                    //'selectMountService' => $request->selectMountService,
                    'selectStatusService' => $request->selectStatusService,
                ]);
            }

            foreach ($orders as $o) {
                $orders->map(function($o){
                    if ($o->customer_branch_id != null) {
                        $branch = DB::table('customer_branches')->where('id', $o->customer_branch_id)->first();
                        $o->empresa = $branch->name;
                        $o->municipality = $branch->municipality;
                    }
                    if ($o->id_status == 1) {
                        $plagues = DB::table('plague_type_quotation as pq')
                            ->join('plague_types as pt', 'pq.plague_type_id', 'pt.id')
                            ->where('pq.quotation_id', $o->id_quotation)->get();

                        $plaguesText = "";
                        foreach ($plagues as $plague) {
                            $plaguesText = $plaguesText . $plague->name . ", ";
                        }
                        $pricesList = DB::table('price_lists')
                            ->where('company_id', $o->companie)
                            ->where('establishment_id', $o->establishment_id)
                            ->first();
                    }
                    if ($o->id_status == 3) {
                        $motive = DB::table('canceled_service_orders')->where('id_service_order', $o->order)->first();
                        $o->motive = $motive->reason . ", " . $motive->commentary;
                    }
                    if ($o->id_status != 3) {
                        $cashes = DB::table('cashes')->where('id_event', $o->id_event)->first();
                        $conditions = DB::table('place_conditions')->where('id_service_order', $o->order)->first();
                        $rating = DB::table('ratings_service_orders')->where('id_service_order', $o->order)->first();

                        if(!empty($cashes)){
                            if($cashes->payment == 1)
                                $o->adeudo = 1;
                            else{
                                $o->adeudo = 2;
                            }
                            if ($cashes->id_payment_way == 1) {
                                $o->credito = 1;
                            }else{
                                $o->credito = 2;
                            }
                        }else{
                            $o->adeudo = null;
                            $o->credito = null;
                        }
                    }
                });
            }
        }

        if ($selectCustomerBranch == null) {
        $events = DB::table('events as ev')
            ->join('employees as em', 'ev.id_employee', 'em.id')
            ->join('service_orders as so', 'ev.id_service_order', 'so.id')
            ->join('quotations as q','so.id_quotation','q.id')
            ->join('customers as c','q.id_customer','c.id')
            ->join('customer_datas as cd','c.id','customer_id')
            ->join('plague_types as pt', 'q.id_plague_jer', 'pt.id')
            ->join('price_lists as pl', 'q.id_price_list', 'pl.id')
            ->select('ev.id','ev.title','ev.initial_hour','ev.final_hour','ev.initial_date',
                'ev.final_date','em.name as employee','so.id_service_order as order','so.total',
                'c.name as client','c.cellphone','cd.address','c.colony','c.municipality','cd.address_number',
                'cd.state','pt.plague_key','em.color', 'ev.id_status')
            ->where('ev.id_status', '!=', 3)
            ->where(function ($query) use ($customer) {
                $query->where('c.customer_main_id', $customer->id)
                    ->orWhere('c.id', $customer->id);
            });
        }

        else {
            $events = DB::table('events as ev')
                ->join('employees as em', 'ev.id_employee', 'em.id')
                ->join('service_orders as so', 'ev.id_service_order', 'so.id')
                ->join('quotations as q','so.id_quotation','q.id')
                ->join('customers as c','q.id_customer','c.id')
                ->join('customer_datas as cd','c.id','customer_id')
                ->join('plague_types as pt', 'q.id_plague_jer', 'pt.id')
                ->join('price_lists as pl', 'q.id_price_list', 'pl.id')
                ->select('ev.id','ev.title','ev.initial_hour','ev.final_hour','ev.initial_date',
                    'ev.final_date','em.name as employee','so.id_service_order as order','so.total',
                    'c.name as client','c.cellphone','cd.address','c.colony','c.municipality','cd.address_number',
                    'cd.state','pt.plague_key','em.color', 'ev.id_status')
                ->where('c.id', $customer->id)
                ->where('ev.id_status', '!=', 3);
        }

        if ($initialDateService != null && $finalDateService != null) {
           $events = $events->whereDate('ev.initial_date', '>=', $initialDateService)
                ->whereDate('ev.initial_date', '<=', $finalDateService);
        }
        if ($selectTechnicianService != 0) $events = $events->where('em.employee_id', $selectTechnicianService);
        if ($selectPlagueService != 0) $events = $events->where('pl.id', $selectPlagueService);
        if ($selectStatusService > 0 && $selectStatusService < 6) $events = $events->where('ev.id_status', $selectStatusService);
        if ($selectStatusService == 6) $events = $events->where('ev.service_type', 1);
        if ($selectStatusService == 7) $events = $events->where('ev.service_type', 3);
        if ($selectStatusService == 8) $events = $events->where('ev.service_type', 2);
        if ($selectStatusService == 9) $events = $events->where('ev.service_type', 4);

        $events = $events->get();

        $technicians = DB::table('employees')->where('id_company', auth()->user()->companie)->where('status', '<>', 200)->get();
        $plagues = DB::table('price_lists')->where('company_id', auth()->user()->companie)->get();
        $customer = DB::table('customers')->where('user_id', auth()->user()->id)->first();
        $customerBranches = DB::table('customers')
            ->where('is_main', 0)
            ->where('customer_main_id', $customer->id)
            ->get();

        return view('dashboard.index')->with([
            'events' => $events,
            'order' => $orders,
            'technicians' => $technicians,
            'plagues' => $plagues,
            'customerBranches' => $customerBranches,
            'selectCustomerBranch' => $selectCustomerBranch
            ]);
    }

    public function inspection(Request $request)
    {
        $customer = DB::table('customers')
            ->where('user_id', auth()->user()->id)
            ->first();
        $selectCustomerBranch = $request->get('customerMonitoring');
        if ($selectCustomerBranch != null) {
            $customerBranch = customer::find($selectCustomerBranch);
            $customer = $customerBranch;
        }

        // Filters Request
        $initialDateService = $request->get('initialDateService');
        if (!$request->exists('initialDateService')) $initialDateService = "null";
        $finalDateService = $request->get('finalDateService');
        if (!$request->exists('finalDateService')) $finalDateService = "null";
        $folio = $request->get('folio');
        if ($folio == null) $folio = 'null';
        $customerMonitoring = $request->get('customerMonitoring');
        $addressCustomerMonitoring = $request->get('addressCustomerMonitoring');

        $inspections = DB::table('station_inspections as si')
            ->join('monitorings as mo', 'si.id_monitoring', 'mo.id')
            ->join('employees as em', 'si.id_technician', 'em.id')
            ->join('service_orders as so', 'si.id_service_order', 'so.id')
            ->join('customers as cu', 'mo.id_customer', 'cu.id')
            ->join('customer_datas as cd', 'cu.id', 'cd.customer_id')
            ->where('mo.id_customer', $customer->id)
            ->where('cu.companie', auth()->user()->companie)
            ->select('si.id_inspection', 'mo.id_monitoring', 'si.date', 'si.hour', 'em.name as technician',
                'cu.name as customer', 'cu.establishment_name', 'cu.colony', 'cu.municipality', 'cd.address',
                'cd.address_number', 'cd.state', 'mo.id as id_monitoring_inc', 'si.id_service_order', 'si.id',
                'so.id_service_order as order');

        if ($folio != 'null' ) $inspections->where('si.id_inspection','like','%'. $folio . '%');
        if ($initialDateService != "null" && $finalDateService != "null") {
            $inspections
                ->whereDate('si.created_at', '>=', $initialDateService)
                ->whereDate('si.created_at', '<=', $finalDateService);
        }
        if ($customerMonitoring != 0) $inspections->where('cu.id', $customerMonitoring);
        if ($addressCustomerMonitoring != 0) $inspections->where('cu.id', $addressCustomerMonitoring);

        $inspectionsFilter = $inspections->orderBy('si.id')->paginate(10)->appends([
            'initialDateService' => $initialDateService,
            'finalDateService' => $finalDateService,
            'folio' => $folio,
            'customerMonitoring' => $customerMonitoring,
            'addressCustomerMonitoring' => $addressCustomerMonitoring
        ]);

        foreach ($inspectionsFilter as $inspection) {
            $inspectionsFilter->map(function($inspection){
                $totals = DB::table('monitoring_trees as mt')
                    ->join('monitoring_nodes as mn', 'mt.id', 'mn.id_monitoring_tree')
                    ->join('type_areas as ta', 'mn.id_type_area', 'ta.id')
                    ->where('mt.id_monitoring', $inspection->id_monitoring_inc)
                    ->where('ta.type', 'station')
                    ->groupBy(['mn.name'])
                    ->select(DB::raw('count(*) as count'), 'mn.name')
                    ->get();
                $inspection->nodes = $totals;
                $inspection->stations = self::getStationsMonitoringByInspection($inspection->id);
            });
        }

        // Filters Inspections
        $customers = customer::where('id', $customer->id)->get();
        $addressCustomers = \Illuminate\Support\Facades\DB::table('customers as c')
            ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
            ->select('c.id', 'cd.address', 'cd.address_number', 'c.colony','cd.state','c.municipality')
            ->where('c.id', $customer->id)
            ->get();

        $customer = DB::table('customers')->where('user_id', auth()->user()->id)->first();
        $customerBranches = DB::table('customers')
            ->where('is_main', 0)
            ->where('customer_main_id', $customer->id)
            ->get();

        return view('dashboard.inspection')->with([
            'selectCustomerBranch' => $selectCustomerBranch,
            'inspections' => $inspectionsFilter,
            'customers' => $customers,
            'customerBranches' => $customerBranches,
            'customer' => $customer,
            'addressCustomers' => $addressCustomers
        ]);
    }

    public function areaInspection(Request $request){
        // Filters Inspections
        $customer = DB::table('customers')
            ->where('user_id', auth()->user()->id)
            ->first();
        $selectCustomerBranch = $request->get('customerArea');
        if ($selectCustomerBranch != null) {
            $customerBranch = customer::find($selectCustomerBranch);
            $customer = $customerBranch;
        }

        // Filters Request
        $initialDateService = $request->get('initialDateService');
        if (!$request->exists('initialDateService')) $initialDateService = "null";
        $finalDateService = $request->get('finalDateService');
        if (!$request->exists('finalDateService')) $finalDateService = "null";
        $folio = $request->get('folio');
        if ($folio == null) $folio = 'null';
        $customerArea = $request->get('customerArea');
        $addressCustomerArea = $request->get('addressCustomerArea');

        $orders =  \Illuminate\Support\Facades\DB::table('service_orders as so')
            ->join('area_inspections as ai', 'ai.id_service_order', 'so.id')
            ->join('quotations as q', 'so.id_quotation', 'q.id')
            ->join('customers as cu', 'q.id_customer', 'cu.id')
            ->join('customer_datas as cd', 'cu.id', 'cd.customer_id')
            ->join('areas as a', 'a.id_customer', 'cu.id')
            ->select('so.id_service_order', 'ai.date_inspection', 'ai.hour_inspection', 'cu.name as customer',
                'cu.establishment_name', 'cu.colony', 'cu.municipality', 'cd.address', 'cd.address_number', 'cd.state',
                'so.id', 'a.id_area', 'cu.id_profile_job_center', 'so.id as id_order','cd.email','cu.cellphone','cu.cellphone_main',
                'a.id_area')
            ->where('cu.id', $customer->id);

        if ($folio != 'null' ) $orders->where('so.id_service_order','like','%'. $folio . '%');
        if ($initialDateService != "null" && $finalDateService != "null") {
            $orders
                ->whereDate('ai.date_inspection', '>=', $initialDateService)
                ->whereDate('ai.date_inspection', '<=', $finalDateService);
        }
        if ($customerArea != 0) $orders->where('cu.id', $customerArea);
        if ($addressCustomerArea != 0) $orders->where('cu.id', $addressCustomerArea);

        $ordersFilter = $orders->groupBy(['so.id_service_order'])->paginate(10)->appends([
            'initialDateService' => $initialDateService,
            'finalDateService' => $finalDateService,
            'folio' => $folio,
            'customerArea' => $customerArea,
            'addressCustomerArea' => $addressCustomerArea
        ]);

        foreach ($ordersFilter as $order) {
            $ordersFilter->map(function ($order) {
                $inspections = \Illuminate\Support\Facades\DB::table('area_inspections as ai')
                    ->where('ai.id_service_order', $order->id)
                    ->get();
                $order->id_encoded = SecurityController::encodeId($order->id);
                $order->count_areas = $inspections->count();
                $company = DB::table('companies')->where('id', Auth::user()->companie)->first();
                $countryCode = DB::table('countries')->where('id', $company->id_code_country)->first();
                $order->urlWhatsappEmailAreaInspection = "https://api.whatsapp.com/send?phone=" . $countryCode->code_country . "{&text=" .
                    "%0d%0dMonitoreo de Áreas: https://pestwareapp.com/area/inspections/pdf/" . $order->id_encoded;
            });
        }

        // Filters Inspections
        $customers = customer::where('id', $customer->id)->get();
        $addressCustomers = \Illuminate\Support\Facades\DB::table('customers as c')
            ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
            ->select('c.id', 'cd.address', 'cd.address_number', 'c.colony','cd.state','c.municipality')
            ->where('c.id', $customer->id)
            ->get();

        $customer = DB::table('customers')->where('user_id', auth()->user()->id)->first();
        $customerBranches = DB::table('customers')
            ->where('is_main', 0)
            ->where('customer_main_id', $customer->id)
            ->get();

        return view('dashboard.areaInspection')->with([
            'selectCustomerBranch' => $selectCustomerBranch,
            'inspections' => $ordersFilter,
            'customers' => $customers,
            'customerBranches' => $customerBranches,
            'customer' => $customer,
            'addressCustomers' => $addressCustomers
        ]);
    }

    public function areasInspectionPdf($id)
    {
        $id = SecurityController::decodeId($id);
        $dataPdfInspection = $this->dataPdfInspection($id);
        $pdf = PDF::loadView('PDF.areaInspectionPdf',$dataPdfInspection)->setPaper('A4');
        return $pdf->stream();
    }

    private function dataPdfInspection($id){
        /*//Validate plan free
        if (Auth::user()->id_plan == $this->PLAN_FREE) {
            SecurityController::abort();
        }*/

        $order = \Illuminate\Support\Facades\DB::table('service_orders as so')
            ->join('events as ev', 'ev.id_service_order', 'so.id')
            ->join('employees as emp', 'ev.id_employee', 'emp.id')
            ->join('area_inspections as ai', 'ai.id_service_order', 'so.id')
            ->join('quotations as q', 'so.id_quotation', 'q.id')
            ->join('customers as cu', 'q.id_customer', 'cu.id')
            ->join('customer_datas as cd', 'cu.id', 'cd.customer_id')
            ->join('areas as a', 'a.id_customer', 'cu.id')
            ->where('so.id', $id)
            ->select('so.id_service_order', 'ai.date_inspection', 'ai.hour_inspection', 'cu.name as customer',
                'cu.establishment_name', 'cu.colony', 'cu.municipality', 'cd.address', 'cd.address_number', 'cd.state',
                'so.id', 'a.id_area', 'ev.start_event', 'ev.final_event', 'emp.name as technician', 'ev.id_job_center',
                'cu.cellphone','cd.email', 'cu.id as customer_id','so.companie')
            ->first();

        $inspections = DB::table('area_inspections as ai')
            ->where('ai.id_service_order', $id)
            ->get();
        $area = Area::where('id_customer', $order->customer_id)->first();
        $inspections = $this->mapInspections($inspections, $area->id);

        // Data for headers report pdf
        $customerFirm = DB::table('service_firms as sf')
            ->select('sf.file_route')
            ->where('sf.id_service_order', $id)->first();
        $date = Carbon::now();
        $idCompanie = $order->companie;
        $image = DB::table('companies')
            ->select('pdf_logo', 'pdf_sello', 'phone', 'licence', 'facebook', 'warnings_service',
                'contract_service', 'pdf_sanitary_license', 'rfc','bussines_name','health_manager','email')
            ->where('id', $idCompanie)
            ->first();
        $jobCenterProfile = profile_job_center::where('id', $order->id_job_center)->first();
        $sanitary_license_qr = env('URL_STORAGE_FTP') . $jobCenterProfile->sanitary_license;
        $addressProfile = DB::table('address_job_centers')->where('profile_job_centers_id', $jobCenterProfile->profile_job_centers_id)->first();

        if ($jobCenterProfile->sanitary_license != null) $qrcode = base64_encode(QrCode::format('svg')->size(90)->errorCorrection('H')->generate($sanitary_license_qr));
        else $qrcode = null;

        return [
            'inspections' => $inspections,
            'images' => $image,
            'customerFirm' => $customerFirm,
            'date' => $date,
            'imagen' => $image,
            'addressProfile' => $addressProfile,
            'jobCenterProfile' => $jobCenterProfile,
            'qrcode' => $qrcode,
            'order' => $order];
    }

    private function mapInspections($inspections, $areaId) {
        foreach ($inspections as $inspection) {
            $inspections->map(function ($inspection) use ($areaId) {
                $area = Area_tree::find($inspection->id_area_node);
                $perimeter = Area_tree::where('id_node', $area->parent)->where('id_area', $areaId)->first();
                $zone = Area_tree::where('id_node', $perimeter->parent)->where('id_area', $areaId)->first();
                $plagues = \Illuminate\Support\Facades\DB::table('area_inspections_plagues as aip')
                    ->join('plague_types as pt', 'aip.id_plague', 'pt.id')
                    ->join('infestation_degrees as id', 'aip.id_infestation_degree', 'id.id')
                    ->select('pt.name as plague', 'id.name as grade')
                    ->where('id_area_inspection', $inspection->id)
                    ->get();
                $photos = AreaInspectionPhotos::where('id_area_inspection', $inspection->id)->get();
                foreach ($photos as $image) {
                    $photos->map(function ($image) {
                        $image->url_s3 = CommonImage::getTemporaryUrl($image->photo, 5);
                    });
                }
                $technician = employee::find($inspection->id_technician);
                $inspection->area = $area->text;
                $inspection->zone = $zone->text;
                $inspection->perimeter = $perimeter->text;
                $inspection->plagues = $plagues;
                $inspection->technician = $technician->name;
                $inspection->photos = $photos;
            });
        }
        return $inspections;
    }

    private function getStationsMonitoringByInspection($inspectionId) {
        $stations = DB::table('check_monitoring_responses as cmr')
            ->join('monitoring_trees as mt', 'cmr.id_station', 'mt.id')
            ->join('monitoring_nodes as mn', 'mt.id', 'mn.id_monitoring_tree')
            ->where('cmr.id_inspection', $inspectionId)
            ->select('mt.id as id_node_tree', 'cmr.*', 'mt.*', 'mn.*', 'cmr.id as id_response_station')
            ->get();
        $stations->map(function($station) use ($inspectionId){
            $perimeter = DB::table('monitoring_trees')
                ->where('id_monitoring', $station->id_monitoring)
                ->where('id_node', $station->parent)
                ->first();
            $perimeterText = $perimeter->text;
            $zone = DB::table('monitoring_trees')
                ->where('id_monitoring', $station->id_monitoring)
                ->where('id_node', $perimeter->parent)
                ->first();
            $zoneText = $zone->text;
            $station->perimeter = $perimeterText;
            $station->zone = $zoneText;
            $conditions = DB::table('station_conditions as sc')
                ->join('monitoring_conditions as mc', 'sc.id_condition', 'mc.id')
                ->where('sc.id_inspection', $inspectionId)
                ->where('sc.id_node', $station->id_node_tree)
                ->select('mc.name')
                ->get();
            $station->conditions = $conditions;
            $plagues = \Illuminate\Support\Facades\DB::table('plagues_response_inspection as pri')
                ->join('plague_types as pt', 'pri.id_plague', 'pt.id')
                ->where('pri.id_station_response', $station->id_response_station)
                ->select('pt.name as plague', 'pri.quantity as quantity_plague')
                ->get();
            $station->plagues = $plagues;
            $station->plagues_count = $plagues->count();
        });
        return $stations;
    }

    public function stationMonitoringPdf($id)
    {
        $isInspection = StationInspection::where('id_service_order', $id)->first();
        if ($isInspection) {
            $inspection = DB::table('station_inspections as si')
                ->join('monitorings as m', 'si.id_monitoring', 'm.id')
                ->join('employees as em', 'si.id_technician', 'em.id')
                ->join('service_orders as so', 'si.id_service_order', 'so.id')
                ->join('events as ev', 'si.id_service_order', 'ev.id_service_order')
                ->join('quotations as qu', 'so.id_quotation', 'qu.id')
                ->join('customers as cu', 'qu.id_customer', 'cu.id')
                ->join('customer_datas as cd', 'cd.customer_id','cu.id')
                ->where('si.id_service_order', $id)
                ->select('si.id', 'si.id_inspection', 'so.id_service_order', 'm.id_monitoring', 'si.date', 'ev.start_event',
                    'ev.final_event', 'em.name as technician', 'em.file_route_firm', 'cu.name as customer', 'so.id_job_center',
                    'cu.establishment_name', 'cd.address','cu.municipality','cu.cellphone','cd.email','cd.billing')
                ->first();

            $groupByStations = self::getStationsMonitoringByInspection($inspection->id);
            $images = DB::table('check_list_images')->where('id_inspection', $inspection->id)->get();
            foreach ($images as $image) {
                $images->map(function ($image) {
                    $image->url_s3 = CommonImage::getTemporaryUrl($image->urlImage, 5);
                });
            }

            $customerFirm = DB::table('service_firms as sf')
                ->select('sf.file_route')
                ->where('sf.id_service_order', $id)->first();

            $firmUrl = "";
            $firmUrlTechnician = "";
            if ($customerFirm) {
                $firmUrl = CommonImage::getTemporaryUrl($customerFirm->file_route, 5);
            }
            if ($inspection->file_route_firm != null) $firmUrlTechnician = CommonImage::getTemporaryUrlPublic($inspection->file_route_firm, 5);

            $date = Carbon::now();
            $idCompanie = Auth::user()->companie;
            $imagen = DB::table('companies')
                ->select('pdf_logo', 'pdf_sello', 'phone', 'licence', 'facebook','warnings_service','contract_service', 'pdf_sanitary_license',
                    'rfc','bussines_name','health_manager','email')->where('id', $idCompanie)->first();
            $jobCenterProfile = profile_job_center::where('id', $inspection->id_job_center)->first();
            $sanitary_license_qr = env('URL_STORAGE_FTP') . $jobCenterProfile->sanitary_license;
            $addressProfile = \Illuminate\Support\Facades\DB::table('address_job_centers')->where('profile_job_centers_id', $jobCenterProfile->profile_job_centers_id)->first();

            if ($jobCenterProfile->sanitary_license != null) $qrcode = base64_encode(QrCode::format('svg')->size(90)->errorCorrection('H')->generate($sanitary_license_qr));
            else $qrcode = null;

            $pdf = PDF::loadView('PDF.stationMonitoringPdf',
                [
                    'stations' => $groupByStations, 'jobCenterProfile' => $jobCenterProfile, 'inspection' => $inspection, 'images' => $images,
                    'customerFirm' => $customerFirm, 'date' => $date, 'imagen' => $imagen, 'addressProfile' => $addressProfile, 'qrcode' => $qrcode, 'firmUrl' => $firmUrl,
                    'firmUrlTechnician' => $firmUrlTechnician
                ])->setPaper('A4');
            return $pdf->stream();
        }
    }

    public function getInfoEvent($id)
    {
        $idOrder = DB::table('events as ev')
            ->join('service_orders as so', 'ev.id_service_order', 'so.id')
            ->join('quotations as qo', 'so.id_quotation', 'qo.id')
            ->join('customers as cu', 'qo.id_customer', 'cu.id')
            ->join('customer_datas as cd', 'cu.id', 'cd.customer_id')
            ->join('employees as em', 'ev.id_employee', 'em.id')
            ->select('cu.name', 'so.id_service_order', 'ev.initial_date', 'ev.initial_hour', 'so.total', 'cd.address as street', 'cd.address_number', 'cu.colony', 'em.name as technical', 'qo.id as id_quotation', 'cu.cellphone', 'cu.municipality', 'cd.state', 'so.id as id_order')
            ->where('ev.id', $id)
            ->first();

        $plagues = DB::table('plague_type_quotation as pq')
            ->join('plague_types as pt', 'pq.plague_type_id', 'pt.id')
            ->where('pq.quotation_id', $idOrder->id_quotation)->get();

        $plaguesText = "";
        foreach ($plagues as $plague) {
            $plaguesText = $plaguesText . $plague->name . ", ";
        }

        $collection = collect($idOrder);
        $collection->put('plagues', $plaguesText);
        $collection->put('initial_date', date('d-m-Y', strtotime($idOrder->initial_date)));
        $collection->put('initial_hour', date('h:i A', strtotime($idOrder->initial_hour)));
        $collection->put('address', $idOrder->street . " #" . $idOrder->address_number . ", " . $idOrder->colony . ". " . $idOrder->municipality . ", " . $idOrder->state);

        return response()->json($collection);
    }

    public function download_pdf($id)
    {
        $certificate = ServiceCertificate::build($id);

        if ($certificate['order']->compania  == 269) {
            $pdf = PDF::loadView('PDF.informCustom', $certificate)->setPaper('A4');
        }
        else {
            $pdf = PDF::loadView('PDF.inform', $certificate)->setPaper('A4');
        }
        return $pdf->stream('Certificadp de Servicio'.' '.$certificate['order']->id_service_order.'.pdf');
    }

    public function serviceOrderPdf($id){
        $serviceOrder = ServiceOrder::build($id);
        $pdf = PDF::loadView('PDF.orderService', $serviceOrder)->setPaper('A4');
        return $pdf->stream('Orden de Servicio'.' '.$serviceOrder['order']->id_service_order.'.pdf');
    }

    public function verModal($id)
    {
        $order = DB::table('events as e')->join('employees as em','e.id_employee','em.id')
            ->join('profile_job_centers as pjc','e.id_job_center','pjc.id')
            ->join('service_orders as so','e.id_service_order','so.id')
            ->join('payment_methods as pm','so.id_payment_method','pm.id')
            ->join('payment_ways as pw','so.id_payment_way','pw.id')
            ->join('users as u','so.user_id','u.id')
            ->join('statuses as st','so.id_status','st.id')
            ->join('quotations as q','so.id_quotation','q.id')
            ->join('establishment_types as et','q.establishment_id','et.id')
            ->join('customers as c','q.id_customer','c.id')
            ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
            ->join('plague_types as pt', 'q.id_plague_jer', 'pt.id')
            ->join('discount_quotation as qd', 'q.id', 'qd.quotation_id')
            ->join('discounts as d', 'qd.discount_id', 'd.id')
            ->join('extras as ex', 'q.id_extra', 'ex.id')
            ->join('price_lists as pl', 'q.id_price_list', 'pl.id')
            ->select('so.id as order','so.id_service_order','so.created_at as date', 'so.observations', 'e.initial_hour','e.initial_date','em.name as nombre', 'e.start_event', 'e.final_event',
                'c.name as cliente','c.establishment_name as empresa','c.cellphone','cd.address','c.municipality',
                'q.id_plague_jer','q.total','pt.plague_key','u.name as agente','st.name as status','cd.address_number','cd.state','cd.email','e.id_status',
                'e.id as event','c.colony','q.id as quotation','e.final_hour','e.final_date','so.address as a_rfc',
                'so.email as e_rfc','so.bussiness_name','so.observations','cd.billing','d.id as discount',
                'd.percentage','ex.id as extra','ex.amount','q.construction_measure','q.garden_measure','q.price',
                'q.establishment_id as e_id','pm.id as pm_id','pw.id as pw_id','pm.name as metodo','pw.name as tipo',
                'cd.email', 'so.companie as compania','et.name as establecimiento', 'so.customer_branch_id', 'so.total as total_order',
                'pl.show_price', 'c.show_price as show_price_customer','em.name')
            ->where('so.id',$id)->first();
        $imagen = DB::table('companies')->select('pdf_logo','pdf_sello','phone','licence','facebook')->where('id',$order->compania)->first();

        //Grado de Infestacion de Plagas
        $plagasGrade = DB::table('place_inspection_plague_type as pt')
            ->join('place_inspections as pi','pt.place_inspection_id','pi.id')
            ->join('plague_types as pl','pt.plague_type_id','pl.id')
            ->join('infestation_degrees as ie','pt.id_infestation_degree','ie.id')
            ->select('pl.name as plaga','ie.name as infestacion')->where('pi.id_service_order',$id)->get();

        //Habit Conditions
        $h = DB::table('habit_condition_service_order as hc')
                ->join('habit_conditions as  h', 'hc.habit_condition_id', 'h.id')
                ->select('h.id', 'h.condition', 'hc.service_order_id','hc.habit_condition_id')
                ->where('hc.service_order_id',$id)->get();
        $habit = collect($h)->toArray();

        //Inhabit Conditions
        $inh = DB::table('inhabitant_type_service_order as it')
                ->join('inhabitant_types as  ih', 'it.inhabitant_type_id', 'ih.id')
                ->select('ih.id', 'ih.name', 'it.service_order_id','it.inhabitant_type_id',
                'it.specification')
                ->where('it.service_order_id',$id)->get();
        $inhabit = collect($inh)->toArray();

        $m = DB::table('mascot_service_order as ms')
            ->join('mascots as  m', 'ms.mascot_id', 'm.id')
            ->select('m.id', 'm.name', 'ms.service_order_id','ms.mascot_id',
            'ms.specification')
            ->where('ms.service_order_id',$id)->get();
        $mascot = collect($m)->toArray();

        $idPlaceInspection = DB::table('place_inspections')->where('id_service_order', $id)->first();
        $images = DB::table('inspection_pictures')->where('place_inspection_id', $idPlaceInspection->id)->select('id', 'file_route')->get();
        $inspection = DB::table('place_inspections')->where('id_service_order', $id)->first();

        foreach ($images as $image) {
            $images->map(function ($image) {
                $image->url_s3 = CommonImage::getTemporaryUrl($image->file_route, 5);
            });
        }

        $idPlaceCondition = DB::table('place_conditions')->where('id_service_order', $id)->first();
        $imagesCon = DB::table('condition_pictures')->where('place_condition_id', $idPlaceCondition->id)->select('id', 'file_route')->get();
        $condition = DB::table('place_conditions')->where('id_service_order', $id)->first();
        $order_cleanings = DB::table('order_cleaning_place_condition as co')
                ->join('place_conditions as pc','co.place_condition_id','pc.id')
                ->join('order_cleanings as o','co.order_cleaning_id','o.id')
                ->select('co.place_condition_id','o.name as cleaning')->where('pc.id_service_order',$id)->get();

        foreach ($imagesCon as $image) {
            $imagesCon->map(function ($image) {
                $image->url_s3 = CommonImage::getTemporaryUrl($image->file_route, 5);
            });
        }

        $idPlaceControl = DB::table('plague_controls')->where('id_service_order', $id)->first();

        $plague_controls = DB::table('plague_controls')->where('id_service_order', $id)->select('control_areas', 'commentary', 'id')->get();

        foreach ($plague_controls as $pg) {
            $plague_controls->map(function($pg){
                $methods_application = DB::table('plague_controls_application_methods as pcam')
                    ->join('application_methods as am', 'pcam.id_application_method', 'am.id')
                    ->select('am.name as method')
                    ->where('plague_control_id', $pg->id)->get();
                $methods = "";
                foreach ($methods_application as $ma) {
                    $methods = $methods . $ma->method . ", ";
                }
                $pg->methods = $methods;
                $plague_controls_products = DB::table('plague_controls_products as pcp')
                    ->join('products as p', 'pcp.id_product', 'p.id')
                    ->where('pcp.plague_control_id', $pg->id)
                    ->select('p.name as product', 'pcp.dose', 'pcp.quantity')
                    ->get();
                $pg->plague_controls = $plague_controls_products;
                $imagesCtrlPlague = DB::table('plague_control_pictures')->where('plague_control_id', $pg->id)->select('id', 'file_route')->get();
                foreach ($imagesCtrlPlague as $image) {
                    $imagesCtrlPlague->map(function ($image) {
                        $image->url_s3 = CommonImage::getTemporaryUrl($image->file_route, 5);
                    });
                }
                $pg->imagesCtrlPlague = $imagesCtrlPlague;
            });
        }
        $plague = DB::table('plague_type_quotation as qpt')->join('plague_types as pt','pt.id','qpt.plague_type_id')
            ->select('pt.name','pt.id as plague')->where('qpt.quotation_id',$order->quotation)->get();
        $plague2 = collect($plague)->toArray();

        $cashe = DB::table('cashes as c')
            ->join('payment_methods as pm', 'c.id_payment_method', 'pm.id')
            ->join('payment_ways as pw', 'c.id_payment_way', 'pw.id')
            ->select('pm.name as payment_method', 'pw.name as payment_type', 'c.amount_received', 'c.commentary', 'c.payment', 'c.id')
            ->where('c.id_service_order', $id)
            ->first();

        $imagesCashe = DB::table('cash_pictures')->where('cash_id', $cashe->id)->get();
        foreach ($imagesCashe as $image) {
            $imagesCashe->map(function ($image) {
                $image->url_s3 = CommonImage::getTemporaryUrl($image->file_route, 5);
            });
        }

        $firm = DB::table('service_firms')->where('id_service_order', $id)->first();
        $firmUrl = "";
        if ($firm) {
            $firmUrl = CommonImage::getTemporaryUrl($firm->file_route, 5);
        }

        $logo = "https://storage.pestwareapp.com/";
        $pdf_logo = $logo.$imagen->pdf_logo;
        $pdf_sello = $logo.$imagen->pdf_sello;

        if ($order->customer_branch_id != null) {
            $branch = DB::table('customer_branches')->where('id', $order->customer_branch_id)->first();
            $order->total = $order->total_order;
            $order->empresa = $branch->name;
            $order->address = $branch->address;
            $order->address_number = $branch->address_number;
            $order->colony = $branch->colony;
            $order->state = $branch->state;
            $order->municipality = $branch->municipality;
        }
        $symbol_country = CommonCompany::getSymbolByCountryWithCompanyId($order->compania);

        return view('dashboard._modaleServiceOrder')->with([
            'order' => $order,
            'plagasGrade' => $plagasGrade,
            'habits' => $habit,
            'inhabits' => $inhabit,
            'mascots' => $mascot,
            'imagesInspection' => $images,
            'imagesCondition' => $imagesCon,
            'inspection' => $inspection,
            'condition' => $condition,
            'order_cleanings' => $order_cleanings,
            'plague_controls' => $plague_controls,
            'cashe' => $cashe,
            'firm' => $firm,
            'imagesCashe' => $imagesCashe,
            'plague2' => $plague2,
            'imagen' => $imagen,
            'pdf_logo'=>$pdf_logo,
            'firmUrl' => $firmUrl,
            'symbol_country' => $symbol_country
        ]);
    }

    public function chart() {
        return view('dashboard.chart');
    }

    public function charts() {
        $customerId = DB::table('customers')->where('user_id', Auth::user()->id)->first();
        return view('dashboard.charts')->with(['customerId' => $customerId->id]);
    }

    public function carpetamip()
    {
        $customer = DB::table('customers')->where('user_id', auth()->user()->id)->first();
        $customerBranches = DB::table('customers')
            ->where('is_main', 0)
            ->where('customer_main_id', $customer->id)
            ->get();

        return view('dashboard.carpetamip')->with(['customerId' => $customer->id, 'customerBranches' => $customerBranches, 'customer' => $customer]);
    }

    public function descarga($id)
    {
        $customer = DB::table('customers as c')
            ->join('customer_datas as cd', 'cd.customer_id', 'c.id')
            ->select('c.id', 'cd.address', 'c.name', 'c.cellphone', 'cd.address_number', 'c.colony', 'c.municipality', 'cd.state', 'cd.email')
            ->where('c.user_id', auth()->user()->id)
            ->first();

        // Create pdf cover page
        $imagen = DB::table('companies')->select('pdf_logo','pdf_sello','phone','licence','facebook')->where('id', auth()->user()->companie)->first();
        $logo = "https://pestwareapp.com/img/";
        $pdf_logo = $logo.$imagen->pdf_logo;

        $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        $fecha = Carbon::now();
        $mes = $meses[($fecha->format('n')) - 1];
        $inputs= $fecha->format('d') . ' de ' . $mes . ' de ' . $fecha->format('Y');
        $elaborado = DB::table('companies as c')
            ->join('contacts as co','c.contact_id','co.id')
            ->select('co.name as responsable')
            ->where('c.id',auth()->user()->companie)->first();
        $portada = PDF::loadView('PDF.portada',['pdf_logo'=>$pdf_logo,'fecha'=>$inputs, 'elaborado' => $elaborado,
            'cliente' => $customer])->save(storage_path() . '/app/mip/' . $customer->id . '/' . $id . '/' . 'PORTADA '. $customer->id . '.pdf');

        $pathCover = 'mip/'.  $customer->id . '/' . $id . '/' . 'PORTADA '. $customer->id . '.pdf';

        // Get files services
        $services = Storage::files('mip/' . $customer->id . '/' . $id . '/services');

        // Get files products
        $products = Storage::files('mip/' . $customer->id . '/' . $id . '/products');

        $cover = [$pathCover];
        $documentsForMip = array_merge($cover, $services);
        $documentsForMip = array_merge($documentsForMip, $products);

        $merger = new Merger(new TcpdiDriver);
        foreach ($documentsForMip as $document) {
            $merger->addFile(storage_path() . '/app/' . $document);
        }
        $mipDoc = $merger->merge();

        $name = 'CARPETA MIP - ' . 'AÑO ' .$id . '.pdf';
        header("Content-type:application/pdf");
        header("Content-Disposition:attachment;filename=$name");
        echo $mipDoc;
        exit;

    }

}
