<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Auth;
use http\Env\Request;

class LoginController extends Controller
{

    public function __constructor()
    {
        $this->middleware('guest',['only', 'showLoginForm']);
    }

    public function showLoginForm()
    {
        return view('auth.login_new');
    }

    public function login()
    {
        $credentials = $this->validate(request(),[
            'cellphone' => 'required|min:7|max:14',
            'password' => 'required|string'
        ]);

        if(Auth::attempt($credentials)){
            return redirect()->route('dashboard');

        }

        return back()
            ->withErrors(['cellphone' => trans('auth.failed')])
            ->withInput(request(['cellphone']));
    }

    public function logout()
    {
        Auth::logout();

        return redirect('/');
    }
}
