<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    public function resetPassword(Request $request)
    {
        $userId = auth()->user()->id;
        $password = $request->get('password');
        $passwordConfirm = $request->get('confirm_password');

        if ($password == null)
            return back()->with(['error' => 'La contraseña no puede ir vacía, intenta de nuevo.']);

        if ($password != $passwordConfirm)
            return back()->with(['error' => 'Las contraseñas no coinciden, intentalo de nuevo.']);

        $passwordEncrypt = bcrypt($password);

        $user = DB::table('users')->where('id', $userId)->update(['password' => $passwordEncrypt]);

        if ($user)
            return back()->with(['message' => 'Se ha cambiado tu contraseña correctamente.']);

        return back()->with(['message' => 'Ha ocurrido un error, verifica tus datos.']);
    }
}
