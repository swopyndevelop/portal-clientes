<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\personal_mail
 *
 * @property int $id
 * @property int $id_company
 * @property int $profile_job_center_id
 * @property string $banner
 * @property string $description
 * @property string|null $image_whatsapp
 * @property string|null $image_messenger
 * @property string $reminder_whatsapp
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|personal_mail newModelQuery()
 * @method static Builder|personal_mail newQuery()
 * @method static Builder|personal_mail query()
 * @method static Builder|personal_mail whereBanner($value)
 * @method static Builder|personal_mail whereCreatedAt($value)
 * @method static Builder|personal_mail whereDescription($value)
 * @method static Builder|personal_mail whereId($value)
 * @method static Builder|personal_mail whereIdCompany($value)
 * @method static Builder|personal_mail whereImageMessenger($value)
 * @method static Builder|personal_mail whereImageWhatsapp($value)
 * @method static Builder|personal_mail whereUpdatedAt($value)
 * @mixin Eloquent
 */
class personal_mail extends Model
{
    protected $table = "personal_mails";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_company',
        'profile_job_center_id',
        'banner',
        'description',
        'image_whatsapp',
        'image_messenger',
        'reminder_whatsapp',
        'email_tracing'
    ];
}
