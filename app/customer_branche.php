<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class customer_branche
 *
 * @package App
 * @author Alberto Martínez
 * @version 12/10/2020
 * @property int $id
 * @property string|null $branch_id
 * @property int $customer_id
 * @property int $id_quotation
 * @property string $name
 * @property string $type
 * @property float|null $total
 * @property string|null $manager
 * @property string|null $phone
 * @property string|null $email
 * @property string $address_number
 * @property string $address
 * @property string $colony
 * @property string $municipality
 * @property string $state
 * @property string $postal_code
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|customer_branche newModelQuery()
 * @method static Builder|customer_branche newQuery()
 * @method static Builder|customer_branche query()
 * @method static Builder|customer_branche whereAddress($value)
 * @method static Builder|customer_branche whereAddressNumber($value)
 * @method static Builder|customer_branche whereBranchId($value)
 * @method static Builder|customer_branche whereColony($value)
 * @method static Builder|customer_branche whereCreatedAt($value)
 * @method static Builder|customer_branche whereCustomerId($value)
 * @method static Builder|customer_branche whereEmail($value)
 * @method static Builder|customer_branche whereId($value)
 * @method static Builder|customer_branche whereManager($value)
 * @method static Builder|customer_branche whereMunicipality($value)
 * @method static Builder|customer_branche whereName($value)
 * @method static Builder|customer_branche wherePhone($value)
 * @method static Builder|customer_branche wherePostalCode($value)
 * @method static Builder|customer_branche whereState($value)
 * @method static Builder|customer_branche whereTotal($value)
 * @method static Builder|customer_branche whereUpdatedAt($value)
 * @mixin Eloquent
 */

class customer_branche extends Model
{
     /**
     * @var string
     */
    protected $table = "customer_branches";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'branch_id',
        'customer_id',
        'id_quotation',
        'name',
        'type',
        'manager',
        'phone',
        'email',
        'address_number',
        'address',
        'colony',
        'municipality',
        'state',
        'postal_code'
    ];
}
