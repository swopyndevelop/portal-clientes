<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ShareServiceOrder
 * @package App
 * @property int $id
 * @property int $id_service_order_main
 * @property int $id_service_order
 * @property int $id_employee
 * @mixin Eloquent
 */
class ShareServiceOrder extends Model
{
    /** * @var string */
    protected $table = "shares_service_order";

    /** * @var array */
    protected $fillable = [
        'id',
        'id_service_order_main',
        'id_service_order',
        'id_employee',
    ];
}
