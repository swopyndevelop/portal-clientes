<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Model;

/**
 * Class StationInspection
 * @package App
 * @property int $id
 * @property string $id_inspection
 * @property int $id_monitoring
 * @property int $id_service_order
 * @property int $id_technician
 * @property string $comments
 * @property string $date
 * @property string $hour
 * @mixin Eloquent
 */
class StationInspection extends Model
{
    /**
     * @var string
     */
    protected $table = "station_inspections";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'id_inspection',
        'id_monitoring',
        'id_service_order',
        'id_technician',
        'comments',
        'date',
        'hour'
    ];
}
