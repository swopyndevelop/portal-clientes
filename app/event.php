<?php

namespace App;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * Class event
 *
 * @package App
 * @author Olga Rodríguez
 * @version 18/04/2019
 * @property int $id
 * @property string $title
 * @property int|null $id_employee
 * @property int|null $id_service_order
 * @property string|null $initial_hour
 * @property string|null $final_hour
 * @property string|null $initial_date
 * @property string|null $final_date
 * @property string|null $start_event
 * @property string|null $final_event
 * @property int $id_job_center
 * @property int $companie
 * @property int|null $id_status
 * @property int|null $x_cut
 * @property string|null $date_cut
 * @property  int|null $id_cut
 * @property int|null $service_type
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|event newModelQuery()
 * @method static Builder|event newQuery()
 * @method static Builder|event query()
 * @method static Builder|event whereCompanie($value)
 * @method static Builder|event whereCreatedAt($value)
 * @method static Builder|event whereDateCut($value)
 * @method static Builder|event whereFinalDate($value)
 * @method static Builder|event whereFinalEvent($value)
 * @method static Builder|event whereFinalHour($value)
 * @method static Builder|event whereId($value)
 * @method static Builder|event whereIdEmployee($value)
 * @method static Builder|event whereIdJobCenter($value)
 * @method static Builder|event whereIdServiceOrder($value)
 * @method static Builder|event whereIdStatus($value)
 * @method static Builder|event whereInitialDate($value)
 * @method static Builder|event whereInitialHour($value)
 * @method static Builder|event whereServiceType($value)
 * @method static Builder|event whereStartEvent($value)
 * @method static Builder|event whereTitle($value)
 * @method static Builder|event whereUpdatedAt($value)
 * @method static Builder|event whereXCut($value)
 * @mixin Eloquent
 */
class event extends Model
{
    /**
     * @var string
     */
    protected $table = "events";

    /**
     * @var array
     */
    protected $fillable = [
        'id',
        'title',
        'id_employee',
        'id_service_order',
        'initial_hour',
        'final_hour',
        'initial_date',
        'final_date',
        'id_job_center',
        'id_status',
        'companie',
        'x_cut',
        'date_cut',
        'id_cut',
        'service_type'
    ];
}
