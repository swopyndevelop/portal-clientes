<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', 'Auth\LoginController@showLoginForm')->middleware('guest');

//Auth
Route::post('login','Auth\LoginController@login')->name('login');
Route::get('logout','Auth\LoginController@logout')->name('logout');
Route::post('reset/password','Auth\ResetPasswordController@resetPassword')->name('reset');

//Dashboard
Route::get('dashboard','DashboardController@index')->name('dashboard');
Route::name('info_event')->get('schedule/event/{id}','DashboardController@getInfoEvent');
Route::name('download')->get('dashboard/{id}/pdf','DashboardController@download_pdf');
Route::name('service_order')->get('service/order/{id}/pdf','DashboardController@serviceOrderPdf');
Route::name('service')->get('dashboard/{id}/service','DashboardController@verModal');

//Chart
Route::get('chart','DashboardController@chart')->name('chart');
Route::post('plagues/grade','ChartController@data')->name('chart_data');
Route::get('reports/plagues', 'DashboardController@charts')->name('charts');

//Folder MIP
Route::name('carpeta')->get('folder', 'DashboardController@carpetamip');
Route::name('download_folder')->get('folder/{id}/download','DashboardController@descarga');

//Inspections
Route::get('inspections','DashboardController@inspection')->name('inspections');
Route::name('station_monitoring_pdf')->get('station/monitoring/pdf/{idOrder}','DashboardController@stationMonitoringPdf');

// Áreas Inspections
Route::get('area/inspections','DashboardController@areaInspection')->name('area_inspections');
Route::name('area_inspection_pdf')->get('area/inspections/pdf/{idOrder}', 'DashboardController@areasInspectionPdf');

//Tracings
Route::get('tracings','TracingController@show')->name('tracings');
Route::name('tracings_create')->post('tracings/store','TracingController@store');
Route::name('getCustomer')->get('tracings/customer/{id}','TracingController@getCustomer');
