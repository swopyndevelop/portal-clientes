import React, {useState, useEffect} from "react";
import ReactDOM from 'react-dom';
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import { Line } from "react-chartjs-2";
import Axios from "axios";
import Moment from 'moment';
import {extendMoment} from 'moment-range';
import {loaderPestWare} from "../loader";
import Swal from "sweetalert2";


const InitApp = () => {
    const dateFirst = new Date()
    const startOfMonth = new Date(dateFirst.getFullYear(), dateFirst.getMonth(), 1);
    const [startDate, setStartDate] = useState(startOfMonth)
    const [endDate, setEndDate] = useState(new Date())
    const [data, setData] = useState({})
    const [didMount, setDidMount] = useState(true)

    const optionsInsectsLine = {
        scales: {
            yAxes: [
                {
                    scaleLabel: {
                        display: true,
                        fontColor: '#5e72e4',
                        labelString: '# de Cebaderos por tipo de consumo'
                    },
                    ticks: {
                        // Include a dollar sign in the ticks
                        callback: function(value, index, values) {
                            return value;
                        },
                        stepSize: 25,
                        min: 0,
                    },
                },
            ],
            xAxes: [
                {
                    scaleLabel: {
                        display: true,
                        fontColor: '#5e72e4',
                        labelString: 'Inspecciones del periodo 01 de Septiembre al 17 de Febrero'
                    },
                },
            ],
        },
        title: {
            display: true,
            fontSize: 14,
            text: 'Gráfica de Tendencia de Plagas'
        }
    }

    useEffect(() => {
        if (didMount) {
            let labelsArrayInsects = []
            let datasetsInsects = []
            let plagues = []
            let grades = []
            let colorsInsects = []
            setData({})
            loaderPestWare('Espere un momento');
            const getData = async () => {

                const response = await Axios.post('../plagues/grade', {
                    initial_date: Moment(startDate).format("YYYY-MM-DD"),
                    final_date: Moment(endDate).format("YYYY-MM-DD"),
                })

                colors = [
                    '#742774',
                    '#2E86C1',
                    '#BA4A00',
                    '#229954',
                    '#D4AC0D',
                    '#2E4053',
                    '#2E4053',
                    '#A93226'
                ]

                response.data.datasets[0].plagues.forEach((plague, i) => {
                    plagues.push(plague)
                    response.data.datasets.forEach((service) => {
                        i == 0 ? labelsArray.push(service.date) : null
                        let grade = 100
                        if (service.plagues[i].grade == "Nulo") {
                            grade = 0
                        } else if (service.plagues[i].grade == "Bajo") {
                            grade = 25
                        } else if (service.plagues[i].grade == "Medio") {
                            grade = 50
                        } else if (service.plagues[i].grade == "Alto") {
                            grade = 75
                        }
                        grades.push(grade)
                    })
                    let obj = {
                        label: plague.plague,
                        data: grades,
                        fill: false,
                        backgroundColor: colors[i],
                        borderColor: colors[i]
                    }
                    datasets.push(obj)
                    grades = []
                })

                let dataSetObj = {
                    labels: labelsArray,
                    datasets: datasets
                }
                setData(dataSetObj)
                Swal.close();

            }
            getData()
        } else {
            setDidMount(false)
        }
    }, [startDate, endDate])

    return (
        <div className="App">
            <div className="row">
                <div className="col-md-6">
                    <label><strong>Período</strong></label>
                    <div id="InitCalendar">
                        <DatePicker
                            selected={startDate}
                            onChange={date => setStartDate(date)}
                            selectsStart
                            startDate={startDate}
                            endDate={endDate}
                        />
                        <DatePicker
                            selected={endDate}
                            onChange={date => setEndDate(date)}
                            selectsEnd
                            startDate={startDate}
                            endDate={endDate}
                            minDate={startDate}
                        />
                    </div>
                </div>
                <div className="col-md-6">
                    <label><strong>Grado de infestación</strong></label>
                    <ul className="nav nav-pills nav-pills-circle" id="tabs_2" role="tablist">
                        <li className="nav-item">
                            <span className="badge badge-pill badge-default">Nulo <b>0</b></span>
                        </li>
                        <li className="nav-item">
                            <span className="badge badge-pill badge-success">Bajo <b>25</b></span>
                        </li>
                        <li className="nav-item">
                            <span className="badge badge-pill badge-info">Medio <b>50</b></span>
                        </li>
                        <li className="nav-item">
                            <span className="badge badge-pill badge-warning">Alto <b>75</b></span>
                        </li>
                        <li className="nav-item">
                            <span className="badge badge-pill badge-danger">Muy Alto <b>100</b></span>
                        </li>
                    </ul>
                </div>
            </div>
            <br/>
            <br/>
            <Line data={data} options={optionsInsectsLine} />
        </div>
    )
}
if (document.getElementById('chart')) {
    ReactDOM.render(<InitApp/>, document.getElementById('chart'))
}
