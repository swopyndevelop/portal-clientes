<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="https://kit.fontawesome.com/64d58efce2.js" crossorigin="anonymous"></script>
    <link rel="icon" type="image/png" href="{{ asset('/img/pestware-cuadre.png') }}" />
    <link rel="stylesheet" href="{{ asset('/loginew/style.css') }}" />
    <link rel="stylesheet" href="{{ asset('/css/estilos/loader-pestware-login.css') }}" />
    <title>PestWare App | Mantenimiento</title>
</head>

<body>
<div class="container">
    <div class="forms-container">
        <div class="signin-signup">
            <form action="{{ route('login') }}" method="POST" class="sign-in-form" id="formLoginNew">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <img src="{{ asset('/loginew/img/logo.svg') }}" class="image" alt="logo" height="200px" style="margin-bottom: 50px;" />
                <h1 class="title-min">Estamos actualizando Pestware Portal para ofrecerte un mejor servicio.</h1>
                <h1 class="title-min">. . .</h1>
            </form>
        </div>
    </div>

    <div class="panels-container">
        <div class="panel left-panel">
            <div class="content">
                <h3>Tenemos algo especial preparado para ti.</h3>
                <p>
                    Estamos deseando enseñartelo.
                </p>
                <p>
                    ¡En un rato estamos de vuelta!
                </p>
            </div>
        </div>
    </div>
</div>
</body>

</html>
