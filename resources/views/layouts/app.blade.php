<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="{{ asset('loginew/img/pestware-cuadre.png') }}" />
    <title>Portal Clientes</title>
    <link href="{{asset('assets/vendor/nucleo/css/nucleo.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
    <link type="text/css" href="{{asset('assets/css/argon.css?v=1.0.0')}}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">

    <!--<link href="{{ asset('plugins/fullcalendar/packages/core/main.css')}}" rel='stylesheet' />
    <link href="{{ asset('plugins/fullcalendar/packages/daygrid/main.css')}}" rel='stylesheet' />
    <link href="{{ asset('plugins/fullcalendar/packages/timegrid/main.css')}}" rel='stylesheet' />

    <script type="text/javascript" src="{{ asset('plugins/fullcalendar/packages/core/main.js')}}"></script>
    <script type="text/javascript" src="{{ asset('plugins/fullcalendar/packages/interaction/main.js')}}"></script>
    <script type="text/javascript" src="{{ asset('plugins/fullcalendar/packages/daygrid/main.js')}}"></script>
    <script type="text/javascript" src="{{ asset('plugins/fullcalendar/packages/timegrid/main.js')}}"></script>
    <script type="text/javascript" src="{{ asset('plugins/fullcalendar/packages/core/locales/es.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js" integrity="sha256-4iQZ6BVL4qNKlQ27TExEhBN1HFPvAvAMbFavKKosSWQ=" crossorigin="anonymous"></script>-->
</head>
<body>
    @if (session()->has('flash'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <span class="alert-icon"><i class="ni ni-like-2"></i></span>
        <span class="alert-text"><strong>Alto!</strong> {{session('flash')}}</span>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    @yield('main-content')

<script src="{{asset('assets/vendor/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('assets/js/argon.js?v=1.0.0')}}"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/lightgallery@1.6.12/dist/css/lightgallery.min.css">
    <script src="https://cdn.jsdelivr.net/npm/lightgallery@1.6.12/dist/js/lightgallery.min.js"></script>
</body>
</html>
