<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Portal Clientes</title>
    <link rel="icon" type="image/png" href="{{ asset('loginew/img/pestware-cuadre.png') }}" />
    <link href="{{asset('assets/vendor/nucleo/css/nucleo.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/@fortawesome/fontawesome-free/css/all.min.css')}}" rel="stylesheet">
    <link type="text/css" href="{{asset('assets/css/argon.css?v=1.0.0')}}" rel="stylesheet">
    <link type="text/css" href="{{asset('css/loader-pestware.css')}}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">

    <link href="{{ asset('assets/plugins/fullcalendar/packages/core/main.css')}}" rel='stylesheet' />
    <link href="{{ asset('assets/plugins/fullcalendar/packages/daygrid/main.css')}}" rel='stylesheet' />
    <link href="{{ asset('assets/plugins/fullcalendar/packages/timegrid/main.css')}}" rel='stylesheet' />

    <link href="{{ asset('css/main.css')}}" rel='stylesheet' />

    <script type="text/javascript" src="{{ asset('assets/plugins/fullcalendar/packages/core/main.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/fullcalendar/packages/interaction/main.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/fullcalendar/packages/daygrid/main.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/fullcalendar/packages/timegrid/main.js')}}"></script>
    <script type="text/javascript" src="{{ asset('assets/plugins/fullcalendar/packages/core/locales/es.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js" integrity="sha256-4iQZ6BVL4qNKlQ27TExEhBN1HFPvAvAMbFavKKosSWQ=" crossorigin="anonymous"></script>
</head>
<body>
    <div class="main-content">
        @include('layouts.sidebar')
        <div class="main-content">
            @include('layouts.htmlheader')
            @yield('main-content')
        </div>
    </div>

<script type="text/javascript" src="{{ asset('js/app.js')}}"></script>

    <script src="{{asset('assets/vendor/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('assets/js/argon.js?v=1.0.0')}}"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/lightgallery@1.6.12/dist/css/lightgallery.min.css">
    <script src="https://cdn.jsdelivr.net/npm/lightgallery@1.6.12/dist/js/lightgallery.min.js"></script>
    <script src="{{ asset('assets/vendor/gallery/lightGallery.js') }}"></script>
    <script src="{{ asset('assets/vendor/dashboard/serviceDashboard.js') }}"></script>
    <script src="{{ asset('assets/vendor/area/index.js') }}"></script>
    <script src="{{ asset('assets/vendor/monitoring/index.js') }}"></script>
    <script src="{{ asset('assets/vendor/tracing/index.js') }}"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
</body>
</html>
