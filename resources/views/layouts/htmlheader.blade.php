<nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
      <div class="container-fluid">
        <!-- Brand -->
        <a id="title-dashboard" class="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="{{route('dashboard')}}"></a>
        <!-- Form -->
        <form class="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex ml-lg-auto">
          <div class="form-group mb-0">
            <!--<div class="input-group input-group-alternative">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-search"></i></span>
              </div>
              <input class="form-control" placeholder="Search" type="text">
            </div>-->
          </div>
        </form>
        <!-- User -->
        <ul class="navbar-nav align-items-center d-none d-md-flex">
          <li class="nav-item dropdown">
            <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <div class="media align-items-center">
                <span class="avatar avatar-sm rounded-circle">
                  <img alt="Image placeholder" src="{{asset('assets/img/theme/avatar.png')}}">
                </span>
                <div class="media-body ml-2 d-none d-lg-block">
                  <span class="mb-0 text-sm  font-weight-bold">{{auth()->user()->name}}</span>
                </div>
              </div>
            </a>
          </li>
        </ul>
          <ul class="navbar-nav ml-lg-auto">
              <li class="nav-item dropdown">
                  <a class="nav-link nav-link-icon" data-toggle="modal" data-target="#modal-form" title="Cambiar Contraseña">
                      <i class="ni ni-lock-circle-open"></i>
                  </a>
              </li>
              <li class="nav-item">
                  <a class="nav-link nav-link-icon" href="{{route('logout')}}" title="Salir">
                      <i class="ni ni-user-run"></i>
                  </a>
              </li>
          </ul>
      </div>
</nav>
@include('auth.changePassword')
