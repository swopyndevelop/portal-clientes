<div class="modal fade" id="inspectionDetail<?php echo $a; ?>" tabindex="-1" role="dialog" aria-labelledby="modal-form" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body p-0">
                <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Detalle de la Inspección</h4>
            </div>
            <br>
            <div class="row container">
                <div class="col-md-6 text-left">
                    <label style="font-weight: bold; color: black;">NO. SERVICIO: </label> {{ $inspection->order }}
                    <br>
                    <label style="font-weight: bold; color: black;">NO. MONITOREO: </label> {{ $inspection->id_monitoring }}
                    <br>
                    <label style="font-weight: bold; color: black;">TÉCNICO APLICADOR: </label> {{ $inspection->technician }}
                </div>
                <div class="col-md-6 text-left">
                    <label style="font-weight: bold; color: black;">FECHA: </label> {{ $inspection->date }}
                    <br>
                    <label style="font-weight: bold; color: black;">HORA: </label> {{ $inspection->hour }}
                </div>
                <div class="col-md-12 table-responsive">
                    <table class="table table-hover" id="tableProduct">
                        <thead>
                        <tr>
                            <th class="text-center">Zona</th>
                            <th class="text-center">Perímetro</th>
                            <th class="text-center">Tipo Estación</th>
                            <th class="text-center">Estación</th>
                            <th class="text-center">Actividad</th>
                            <th class="text-center">Condiciones</th>
                            <th class="text-center">Observaciones</th>
                            <th class="text-center">Acciones Correctivas</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($inspection->stations as $station)
                            <tr>
                                <td class="text-center">{{ $station->zone }}</td>
                                <td class="text-center">{{ $station->perimeter }}</td>
                                <td class="text-center">{{ $station->name }}</td>
                                <td class="text-center">{{ $station->text }}</td>
                                <td class="text-center">
                                    @if($station->value == "Sin Ingesta" || $station->value == "Sin Captura" || $station->value == "Captura Baja")
                                        <span style="color: green; font-weight: bold">{{ $station->value }}</span>
                                    @elseif($station->value == "Ingesta Parcial" || $station->value == "Captura Media")
                                        <span style="color: orange; font-weight: bold">{{ $station->value }}</span>
                                    @elseif($station->value == "Ingesta Total" || $station->value == "Captura Alta" || $station->value == "Captura Muy Alta")
                                        <span style="color: red; font-weight: bold">{{ $station->value }}</span>
                                    @else
                                        <span style="color: black; font-weight: bold">{{ $station->value }}</span>
                                    @endif
                                </td>
                                <td class="text-center">
                                    @foreach($station->conditions as $condition)
                                        {{ $condition->name }}
                                    @endforeach
                                </td>
                                <td class="text-center">{{ $station->actions }}</td>
                                <td class="text-center">{{ $station->comments }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
