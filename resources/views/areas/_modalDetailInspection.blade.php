<!-- INICIA MODAL PARA MOSTRAR DETALLE DEL MONITOREO -->
<div class="modal fade" id="showAreaInspectionModal" tabindex="-1">
    <div class="modal-dialog modal-lg" style="width: 90% !important;">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <h4 class="modal-title text-center col-lg-12 text-info" id="modalTitle">Detalle de la Inspección</h4>
                <div class="row container">
                    <div class="col-md-6">
                        <label style="font-weight: bold; color: black;">NO. SERVICIO: </label> <span id="labelNoService"></span>
                        <br>
                        <label style="font-weight: bold; color: black;">NO. AREA: </label> <span id="labelIdArea"></span>
                        <br>
                        <label style="font-weight: bold; color: black;">FECHA: </label> <span id="labelDate"></span>
                        <br>
                        <label style="font-weight: bold; color: black;">HORA: </label> <span id="labelHour"></span>
                    </div>
                </div>
                <div class="row" style="margin: 10px;">
                    <div class="col-md-12 table-responsive">
                        <table class="table table-hover" id="tableProduct">
                            <thead>
                            <tr>
                                <th class="text-center">Zona</th>
                                <th class="text-center">Perímetro</th>
                                <th class="text-center">Área</th>
                                <th class="text-center">Técnico</th>
                                <th class="text-center">Comentarios</th>
                                <th>Plagas / Grado</th>
                                <th class="text-center">Hora</th>
                                <th class="text-center">Evidencias</th>
                            </tr>
                            </thead>
                            <tbody id="tableBodyDetailInspection">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- TERMINA MODAL PARA MOSTRAR DETALLE DEL MONITOREO -->