<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Carpeta </title>
</head>
<style>
	body{
        font-family: 'MontserratBold', Helvetica, Arial, sans-serif
	}
  .column {
  float: left;
  width: 50%;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}
</style>
<body>
    <div class="row">
        <div class="column">
            <p><span style="font-weight: bold">Nombre:</span> {{$cliente->name}}</p>
            <p><span style="font-weight: bold">Celular:</span> {{$cliente->cellphone}}</p>
            <p><span style="font-weight: bold">Dirección:</span> {{$cliente->address}} {{$cliente->address_number}}, {{$cliente->colony}}, {{$cliente->municipality}}, {{$cliente->state}}</p>
            <p><span style="font-weight: bold">E-mail:</span> {{$cliente->email}}</p>
            <br><br><br><br><br><br><br>
            <img src="{{$pdf_logo}}" alt="" width="97%">
        </div>
        <div class="column" align="center">
            <div style="background-color: gray">
                <br><br><br><br><br><br><br>
                <br><br><br><br><br><br><br>
                <br>
                <h1 style="font-size: 30pt; font-weight:bold; color:white">PLAN MAESTRO</h1>
                <p style="font-size: 25pt; color:white">Manejo Integral de Plagas</p>
                <br><br><br><br><br><br><br>
                <p style="font-size: 15pt; color:white">Investigación, procesos, formatos y documentación necesaria para
                    garantizar servicios eficaces y de calidad.
                </p>
                <br><br><br><br><br>
                <p style="font-size: 12pt; color:white">Fecha de Elaboración: {{$fecha}}</p>
                <p style="font-size: 12pt; color:white">Elaborado por: {{$elaborado->responsable}}</p>
                <br>
            </div>
        </div>
      </div>
</body>
</html>
