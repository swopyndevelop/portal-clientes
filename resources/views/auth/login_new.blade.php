<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="https://kit.fontawesome.com/64d58efce2.js" crossorigin="anonymous"></script>
    <link rel="icon" type="image/png" href="{{ asset('loginew/img/pestware-cuadre.png') }}" />
    <link rel="stylesheet" href="{{ asset('/loginew/style.css') }}" />
    <title>Portal Clientes | PestWare App</title>
</head>

<body>
<div class="container sign-up-mode">
    <div class="forms-container">
        <div class="signin-signup">
            <form action="{{route('login')}}" method="post" class="sign-up-form">
                {{csrf_field()}}
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <h2 class="title">¡Bienvenido!</h2>
                <div class="input-field">
                    <i class="fas fa-phone-alt"></i>
                    <input type="text" placeholder="Teléfono" name="cellphone" id="cellphone" />
                </div>
                {!! $errors->first('cellphone','<h5 style="color: red;"><strong>Error: </strong> :message</h5>')!!}
                <div class="input-field" id="inputPassword">
                    <i class="fas fa-lock" id="iconPassword"></i>
                    <input id="password" name="password" autocomplete="off" type="password" placeholder="Contraseña" />
                    <span style="position: absolute; right: 15px; transform: translate(0,-50%); top: 50%; cursor: pointer;">
			            <i class="fas fa-eye" aria-hidden="true" id="eye" onclick="toggle()"></i>
		            </span>
                </div>
                <div id="errors-register">
                    {!! $errors->first('password','<h5 style="color: red;"><strong>Error: </strong> :message</h5>')!!}
                </div>
                <input type="submit" class="btn" value="Entrar >" />
            </form>
        </div>
    </div>

    <div class="panels-container">
        <div class="panel left-panel">
            <div class="content">
                <h3>¿Eres nuevo en PestWare App?</h3>
                <p>
                    ¡ Aprovecha nuestro período de prueba gratis y descubre lo que Pestware App tiene para ti !
                </p>
                <button class="btn transparent" id="sign-up-btn">
                    Comienza
                </button>
            </div>
            <img src="{{ asset('/loginew/img/log.svg') }}" class="image" alt="" />
        </div>
        <div class="panel right-panel">
            <div class="content">
                <h3>Portal de Clientes</h3>
                <p><b style="font-weight: bold;">¡Bienvenido! </b>Aquí podrás consultar toda la información relacionada a tus servicios
                    de control de plagas como:
                </p>
                <p>
                    Agendas de Trabajo, Carpetas Operativas, Informes de Servicio, Certificados y Constancias,
                    Reportes y Gráficas de Tendencia, Diplomas de Capacitación de Técnicos, Documentación para Auditorias Internas,
                    Hojas de Seguridad  y Fichas Técnicas de Plaguicidas.
                </p>
            </div>
            <img src="{{ asset('/loginew/img/register.svg') }}" class="image" alt="" />
        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.33.1/dist/sweetalert2.all.min.js"></script>
<script>
    let state= false;
    function toggle(){
        if(state){
            document.getElementById("password").setAttribute("type","password");
            state = false;
        }
        else{
            document.getElementById("password").setAttribute("type","text");
            state = true;
        }
    }
</script>
</body>

</html>

