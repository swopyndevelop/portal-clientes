@extends('layouts.app')

@section('main-content')
    <div class="main-content">
        <!-- Navbar -->
        <nav class="navbar navbar-top navbar-horizontal navbar-expand-md navbar-dark">
            <div class="container ml-2">
                <img class="img-left" src="{{asset('assets/img/login/title.png')}}" alt="logo" height="50">
            </div>
        </nav>
        <!-- Header -->
        <div class="header bg-gradient-primary py-7 py-lg-8">
            <div class="container">
                <div class="header-body text-center mb-4">
                    <div class="row justify-content-center">
                        <div class="col-lg-5 col-md-6">
                            <h1 class="text-white">¡Bienvenido!</h1>
                            <p class="text-lead text-light" id="title-login"></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="separator separator-bottom separator-skew zindex-100">
                <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1"
                     xmlns="http://www.w3.org/2000/svg">
                    <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
                </svg>
            </div>
        </div>
        <!-- Page content -->
        <div class="container mt--8 pb-1">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-7">
                    <div class="card bg-secondary shadow border-0">
                        <div class="card-header bg-transparent pb-3">
                            <div class="card-body px-lg-5 py-lg-5">
                                <div class="text-center text-muted mb-4">
                                    <small>Iniciar Sesión</small>
                                    <img id="logo-auth" class="img-center" src="{{asset('assets/img/pestwareapp.jpeg')}}" alt="logo" height="100">
                                </div>
                                <form method="POST" action="{{route('login')}}">
                                    {{csrf_field()}}

                                    <div class="form-group mb-3 {{$errors->has('cellphone') ? 'has-error' : ''}}">
                                        <div class="input-group input-group-alternative">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i
                                                        class="ni ni-mobile-button"></i></span>
                                            </div>
                                            <input class="form-control" placeholder="Número" type="number"
                                                   name="cellphone" id="cellphone">
                                        </div>
                                        {!! $errors->first('cellphone','<h5 class="text-danger"><strong>Error: </strong> :message</h5>')!!}
                                    </div>
                                    <div class="form-group {{$errors->has('password') ? 'has-error' : ''}}">
                                        <div class="input-group input-group-alternative">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i
                                                        class="ni ni-lock-circle-open"></i></span>
                                            </div>
                                            <input class="form-control" placeholder="Contraseña" type="password"
                                                   name="password" id="password">
                                        </div>
                                        {!! $errors->first('password','<h5 class="text-danger"><strong>Error: </strong> :message</h5>')!!}
                                    </div>
                                    <div class="custom-control custom-control-alternative custom-checkbox">
                                        <input class="custom-control-input" id=" customCheckLogin" type="checkbox">
                                        <label class="custom-control-label" for=" customCheckLogin">
                                            <span class="text-muted">Recordarme</span>
                                        </label>
                                    </div>
                                    <div class="text-center">
                                        <button class="btn btn-primary my-4">Iniciar Sesión</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer -->
        <footer class="py-5">
            <div class="container ml-0">
                <div class="row align-items-center justify-content-xl-between">
                    <div class="col-xl-6">
                        <div class="copyright text-center text-xl-left text-muted">
                            &copy; 2020 <a href="https://pestwareapp.com/" class="font-weight-bold ml-1"
                                target="_blank">powered by PestWareApp </a>Developed <span>Swopyn</span>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection

<script>
    document.addEventListener('DOMContentLoaded', function () {
        // Client domains list
        let domainsList = [
            {
                'domain': 'portal-clientes.test',
                'logo': '9L29sONm0hmFWP1RKHblQkbrLqUC4abYqYJ7Bk0M.png',
                'title': 'Swopyn'
            },
            {
                'domain': 'biofin-portal-clientes.pestwareapp.com',
                'logo': 'Ybp20XMLA4bJ2pCXe9r5dEQYBDcvBz3FE1AZ0EAB.jpeg',
                'title': 'BIOFIN Fumigaciones Orgánicas'
            }
        ];

        let urlBase = 'https://pestwareapp.com/img/logos/';
        let titleBase = 'Acceso al Portal de Clientes de ';
        let domain = window.location.hostname;

        domainsList.forEach(item => {
            if (domain === item.domain) {
                document.getElementById("logo-auth").src = urlBase + item.logo;
                document.getElementById("title-login").innerHTML = titleBase + item.title;
            }
        })
    });
</script>
