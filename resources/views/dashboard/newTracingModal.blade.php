{{--
<!-- Modal -->
<div class="modal fade" id="modalCreateTracing" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Nueva Solicitud de Seguimeinto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route('tracings_create') }}" enctype="multipart/form-data">
                <div class="modal-body">
                    {{csrf_field()}}
                    <h6 for="form" class="form-control-label">Redactar solicitud:</h6>
                    <textarea id="comments" name="comments" class="form-control form-control-alternative" rows="7" placeholder="Escribe aquí tu solicitud ..." required></textarea>
--}}
{{--
                    <label for="selectCustomerBranch">Sucursal</label>
                    <select id="selectCustomerBranch" name="selectCustomerBranch" class="form-control" style="width: 100%;">
                        <option value="">
                            Principal
                        </option>

                       @foreach($customerBranches as $branch)
                            <option value="{{ $branch->id }}" {{ $branch->id == $selectCustomerBranch ? 'selected' : '' }}>
                                {{ $branch->name }}
                            </option>
                        @endforeach
                    </select>--}}{{--

                    <h6 for="form" class="form-control-label">Evidencia:</h6>
                    <input type="file" name="evidence" id="evidence" accept="image/png, image/jpeg, image/jpg">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary btn-sm">Solicitar</button>
                </div>
            </form>
        </div>
    </div>
</div>
--}}


<!-- Modal -->
<div class="modal fade" id="modalCreateTracing" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Nueva Solicitud de Seguimiento</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ Route('tracings_create') }}" method="POST" id="formTracingByBranch" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="box-body text-center">
                        <textarea id="tracingComments" name="tracingComments" class="form-control form-control-alternative"
                                  rows="7" placeholder="Escribe aquí tu solicitud ..." required>
                        </textarea>
                        <label for="selectCustomerBranches">Cliente/Sucursal</label>
                        <select id="selectCustomerBranches" name="selectCustomerBranches" class="form-control" style="width: 100%;"></select>
                        <br>
                        <input class="form-control" type="email" name="emailTracingOne" id="emailTracingOne" placeholder="ejemplo@example.com">
                        <br>
                        <input class="form-control" type="email" name="emailTracingTwo" id="emailTracingTwo" placeholder="ejemplo2@example.com">
                        <br>
                        <input type="file" name="evidence" id="evidence" accept="image/png, image/jpeg, image/jpg">
                    </div>
                    <div class="modal-footer">
                        <div class="text-center">
                            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancelar</button>
                            <button class="btn btn-primary btn-sm" type="submit" id="saveButtonTracingByBranch">Guardar</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>
