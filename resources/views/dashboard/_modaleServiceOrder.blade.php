@extends('layouts.app2')
@section('main-content')
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
    <div class="container-fluid">
        <div class="header-body">
        @if(session()->has('message'))
          <div class="alert alert-success alert-dismissible fade show" role="alert">
              <span class="alert-icon"><i class="ni ni-like-2"></i></span>
              <span class="alert-text"><strong>Aviso: </strong> {{ session()->get('message') }}</span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
        @endif
        @if(session()->has('error'))
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
              <span class="alert-icon"><i class="ni ni-like-2"></i></span>
              <span class="alert-text"><strong>Error: </strong> {{ session()->get('error') }}</span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
        @endif
        </div>
    </div>
</div>
 <!-- Page content -->
 <div class="container-fluid mt--7">
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header bg-transparent border-0">
              <h3 class="text-black mb-0">Orden de Servicio</h3>
            </div>
            <div class="table-responsive">
              <div class="container">
                <div class="row">
                  <div class="col-md-6">
                    <img src="{{$pdf_logo}}" alt="logo" width="200px" height="200px">
						        <p><span style="color:black;font-size:9pt">Licencia No.{{$imagen->licence}} | {{$imagen->phone}} | Facebook: {{$imagen->facebook}}</span></p>
						        <p><span style="font-size:12pt;color:black;"> Atención a {{$order->cliente}}</span>
							      <br><span style="font-size:10.5pt;font-weight:bold;color:black;">{{$order->empresa}}</span>
							      <br><span style="font-size:8pt;color:black;">{{$order->address}} #{{$order->address_number}},
							          {{$order->colony}}, {{$order->municipality}}, {{$order->state}}</span>
							      <br><span style="font-size:8pt;color:black;"> Tel. {{$order->cellphone}} </span><br>
							      @if($order->email != null)
								      <span style="font-size:8pt;color:black;"> {{$order->email}}</span></p>
							      @endif
                  </div>
                  <div class="col-md-6 text-right">
                      <p style="margin-bottom: 2px; margin-top: 0px">
                          <span style="font-size:8pt; font-weight: bold">NO. SERVICIO: </span>
                          <span style="color:red;font-weight:bold;font-size:8pt;">{{$order->id_service_order}}</span>
                      </p>
                      <p style="margin-bottom: 2px; margin-top: 2px">
                          <span style="font-size:8pt; font-weight: bold">TIPO DE SERVICIO: </span>
                          <span style="font-size:8pt;">{{$order->establecimiento}}</span>
                      </p>
                      @if($order->show_price == 1 || $order->show_price_customer == 1)
                          @if($order->show_price_customer == 1 && $order->show_price == 0)
                              <p style="margin-bottom: 2px; margin-top: 2px">
                                  <span style="font-size:8pt; font-weight: bold">IMPORTE: </span>
                                  <span style="font-size:8pt;">{{ $symbol_country }}{{$order->total}}</span>
                              </p>
                          @elseif($order->show_price_customer == 1 && $order->show_price == 1)
                              <p style="margin-bottom: 2px; margin-top: 2px">
                                  <span style="font-size:8pt; font-weight: bold">IMPORTE: </span>
                                  <span style="font-size:8pt;">{{ $symbol_country }}{{$order->total}}</span>
                              </p>
                          @endif
                      @endif
                      <p style="margin-bottom: 2px; margin-top: 2px">
                          <span style="font-size:8pt; font-weight: bold">FECHA: </span>
                          <span style="font-size:8pt;"><?php echo date("d/m/y",strtotime($order->initial_date))?></span>
                      </p>
                      <p style="margin-bottom: 2px; margin-top: 2px">
                          <span style="font-size:8pt; font-weight: bold">HORA DE ENTRADA: </span>
                          <span style="font-size:8pt;"><?php echo date("H:i",strtotime($order->start_event))?></span>
                      </p>
                      <p style="margin-bottom: 2px; margin-top: 2px">
                          <span style="font-size:8pt; font-weight: bold">HORA DE SALIDA: </span>
                          <span style="font-size:8pt;"><?php echo date("H:i",strtotime($order->final_event))?></span>
                      </p>
                      <p style="margin-bottom: 2px; margin-top: 2px">
                      <p style="margin-bottom: 2px; margin-top: 2px">
                          <span style="font-size:8pt; font-weight: bold">TÉCNICO RESPONSABLE: </span>
                          <span style="font-size:8pt;">{{$order->name}}</span>
                      </p>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-11" style="margin:auto;border-bottom: 2px solid;border-color:black;margin-left:45px;margin-right:40px">
                    <span style="font-size:11pt;font-weight:bold;color:black">DETALLE DEL SERVICIO </span>
                  </div>
                  <div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;">
                    <br>
                    <p><span style="font-size:11pt;font-weight:bold;color:black">PLAGAS:</span>
                      <span style="font-size:11pt;color:black">
                        @foreach($plague2 as $item) {{$item->name}}, @endforeach
                      </span>
                    </p>
                  </div>
                  <div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;">
                    <br>
                    <p><span style="font-size:11pt;font-weight:bold;color:black">CONDICIONES DE QUIEN HABITA:</span>
                      <span style="color:black">@foreach($habits as $habit) {{ $habit->condition }},@endforeach
                                    @foreach($inhabits as $inhabit) {{ $inhabit->name }},@endforeach
                                    @foreach($mascots as $mascot) {{ $mascot->name }},@endforeach </span>
                    </p>
                  </div>
                  <div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;">
                    <br>
                    <p><span style="font-size:11pt;font-weight:bold;color:black">COMENTARIOS:</span>
                      <span style="font-size:11pt;">
                        {{ $order->observations }}
                      </span>
                    <p>
                  </div>
                </div>
                <br><br>
                <div class="row">
                  <div class="col-lg-11" style="margin:auto;border-bottom: 2px solid;border-color:black;margin-left:45px;margin-right:40px">
                    <span style="font-size:11pt;font-weight:bold;color:black">INSPECCION DEL LUGAR </span>
                  </div>
                  <div class="col-lg-11" style="border-bottom: 1px solid;border-color:black;margin-left:45px;margin-right:40px;">
                    <br>
                    <p><span style="font-size:11pt;font-weight:bold;color:black">PLAGAS:</span>
                      <span style="font-size:11pt;color:black">
                        @foreach($plagasGrade as $pg) {{ $pg->plaga }}: {{ $pg->infestacion }},@endforeach
                      </span>
                    </p>
                  </div>
                  <div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;">
                    <br>
                    <p><span style="font-size:11pt;font-weight:bold;color:black">AREAS DE ANIDAMIENTO:</span>
                      <span style="font-size:11pt;color:black">
                        {{ $inspection->nesting_areas }}
                      </span>
                    </p>
                  </div>
                  <div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;">
                    <br>
                    <p><span style="font-size:11pt;font-weight:bold;color:black">COMENTARIOS:</span>
                      <span style="font-size:11pt;color:black">
                        {{ $inspection->commentary }}
                      </span>
                    </p>
                  </div>
                  <div class="col-lg-11" style="border-bottom: 1px solid;border-color:black;margin-left:45px;margin-right:40px;" id="selector1">
                    <br>
                    <p><span style="font-size:11pt;font-weight:bold;color:black">FOTOS:</span>
                        @foreach($imagesInspection as $imagIns)
                            <span class="item" data-src="{{ $imagIns->url_s3 }}">
									<img src="{{ $imagIns->url_s3 }}" height="100" width="100">
								</span>
                        @endforeach
                    </p>
                  </div>
                </div>
                <br><br>
				<div class="row">
					<div class="col-lg-11" style="margin:auto;border-bottom: 2px solid;border-color:black;margin-left:45px;margin-right:40px">
						<span style="font-size:11pt;font-weight:bold;color:black">CONDICIONES DEL LUGAR </span>
					</div>
					<div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;">
						<br>
						<p><span style="font-size:11pt;font-weight:bold;color:black">CUMPLIO CON LAS INDICACIONES:</span>
							<span style="font-size:11pt;color:black">
								@if($condition->indications == 1)Si @elseif($condition->indications == 2)No @else Una parte @endif
							</span>
						</p>
					</div>
					<div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;">
						<br>
						<p><span style="font-size:11pt;font-weight:bold;color:black">ORDEN Y LIMPIEZA:</span>
							<span style="font-size:11pt;color:black">
								@foreach($order_cleanings as $oc) {{ $oc->cleaning }}, @endforeach
							</span>
						</p>
					</div>
					<div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;">
						<br>
						<p><span style="font-size:11pt;font-weight:bold;color:black">ACCESOS RESTRINGIDOS:</span>
							<span style="font-size:11pt;color:black">
								{{ $condition->restricted_access }}
							</span>
						</p>
					</div>
					<div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;">
						<br>
						<p><span style="font-size:11pt;font-weight:bold;color:black">COMENTARIOS:</span>
							<span style="font-size:11pt;color:black">
								{{ $condition->commentary }}
							</span>
						</p>
					</div>
					<div class="col-lg-11"  style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;" id="selector2">
						<br>
						<p><span style="font-size:11pt;font-weight:bold;color:black">FOTOS:</span>
                            @foreach($imagesCondition as $imagCon)
                                <span class="item" data-src="{{ $imagCon->url_s3 }}">
							    	<img src="{{ $imagCon->url_s3 }}" height="100" width="100">
								</span>
                            @endforeach
						</p>
					</div>
        </div>
        <br><br>
				<div class="row">
					<div class="col-lg-11" style="margin:auto;border-bottom: 2px solid;border-color:black;margin-left:45px;margin-right:40px">
						<span style="font-size:11pt;font-weight:bold;color:black">CONTROL DE PLAGAS</span>
					</div>
					@foreach($plague_controls as $pc)
						<div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;">
							<br>
							<p><span style="font-size:11pt;font-weight:bold;color:black">AREA A CONTROLAR:</span>
								<span style="font-size:11pt;color:black">
									{{ $pc->control_areas }}
								</span>
							</p>
						</div>
						<div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;">
							<br>
							<p><span style="font-size:11pt;font-weight:bold;color:black">METODOS DE APLICACION:</span>
								<span style="font-size:11pt;color:black">
									{{ $pc->methods }}
								</span>
							</p>
						</div>
						<div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;">
							<br>
							<p><span style="font-size:11pt;font-weight:bold;color:black">COMENTARIOS:</span>
								<span style="font-size:11pt;color:black">
									{{ $pc->commentary }}
								</span>
							</p>
						</div>
						<br>
						<div class="col-lg-11" style="margin-right:40px;margin-left:45px;font-size:11pt">
							<table class="table table-bordered table-responsive">
								<thead>
									<tr>
										<td style="color:black;font-weight:bold">PLAGUICIDA APLICADO</td>
										<td style="color:black;font-weight:bold">DOSIS</td>
										<td style="color:black;font-weight:bold">CANTIDAD</td>
									</tr>
								</thead>
								<tbody>
									@foreach($pc->plague_controls as $pcp)
									<tr>
										<td style="font-size:11">{{$pcp->product}}</td>
										<td style="font-size:11">{{$pcp->dose}}</td>
										<td style="font-size:11">{{$pcp->quantity}}</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
						<div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;" id="selector3">
							<br>
							<p><span style="font-size:11pt;font-weight:bold;color:black">FOTOS:</span>
                                @foreach($pc->imagesCtrlPlague as $imagCtrl)
                                    <span class="item" data-src="{{ $imagCtrl->url_s3 }}">
								    		<img src="{{ $imagCtrl->url_s3 }}" height="100" width="100">
										</span>
                                @endforeach
								</p>
						</div>
					@endforeach
        </div>
        <br><br>
				<div class="row">
					<div class="col-lg-11" style="margin:auto;border-bottom: 2px solid;border-color:black;margin-left:45px;margin-right:40px">
						<span style="font-size:11pt;font-weight:bold;color:black">PAGO DEL SERVICIO</span>
					</div>
					<div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;">
						<br>
						<p><span style="font-size:11pt;font-weight:bold;color:black">IMPORTE:</span>
							<span style="font-size:11pt;color:black">
								${{ $order->total }}
							</span>
						</p>
					</div>
					<div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;">
						<br>
						<p><span style="font-size:11pt;font-weight:bold;color:black">METODO DE PAGO:</span>
							<span style="font-size:11pt;color:black">
								{{ $cashe->payment_method }}
							</span>
						</p>
					</div>
					<div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;">
						<br>
						<p><span style="font-size:11pt;font-weight:bold;color:black">TIPO DE PAGO:</span>
							<span style="font-size:11pt;color:black">
								{{ $cashe->payment_type }}
							</span>
						</p>
					</div>
					<div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;">
						<br>
						<p><span style="font-size:11pt;font-weight:bold;color:black">IMPORTE RECIBIDO:</span>
							<span style="font-size:11pt;color:black">
								${{ $cashe->amount_received }}
							</span>
						</p>
					</div>
					<div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;">
						<br>
						<p><span style="font-size:11pt;font-weight:bold;color:black">COMENTARIOS:</span>
							<span style="font-size:11pt;color:black">
								{{ $cashe->commentary }}
								@if($cashe->payment == 1)
									<strong style="color: #b71c1c">*El cliente no pago.</strong>
								@endif
							</span>
						</p>
					</div>
					<div class="col-lg-11" style="margin-left:45px;margin-right:40px;border-bottom: 1px solid;border-color:black;" id="selector4">
						<br>
						<p><span style="font-size:11pt;font-weight:bold;color:black">FOTOS:</span>
                            @foreach($imagesCashe as $imageCashe)
                                <span class="item" data-src="{{ $imageCashe->url_s3 }}">
									<img src="{{ $imageCashe->url_s3 }}" height="100" width="100">
								</span>
                            @endforeach
						</p>
					</div>
        </div>
        <br><br>
				<div class="row" align="center">
                    <div class="col-md-12">
                        <h4 style="margin-left: 15px; margin-top: 0px !important; color:black; text-align: center;">FIRMA DEL CLIENTE</h4>
                        @if($firm)
                            <img src="{{ $firmUrl }}" height="400" width="300" class="img-thumbnail">
                        @endif
                        <br>
                        <label style="font-weight: bold; color: black;">{{ $order->cliente }}</label>
                        @if($firm)
                            @if($firm->other_name != null)<br><span style="text-align:center;font-size:9pt;font-weight:bold">{{$firm->other_name}}</span><br>@endif
                        @endif
				</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<br><br><br><br><br>
@endsection
<script>
    document.addEventListener('DOMContentLoaded', function() {
        $('#selector1').lightGallery({
            selector: '.item'
        });
        $('#selector2').lightGallery({
            selector: '.item'
        });
        $('#selector3').lightGallery({
            selector: '.item'
        });
        $('#selector4').lightGallery({
            selector: '.item'
        });
    });
</script>
