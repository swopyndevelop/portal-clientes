@extends('layouts.app2')

@section('main-content')
    <div class="header bg-gradient-primary pb-6 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">
            </div>
        </div>
    </div>
    <div class="container-fluid mt--7">
        <input type="hidden" value="{{ $customerId }}" id="customerId">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <h3 class="mb-0">Reporte Gráfico de Monitoreo de Estaciones y Control de Plagas</h3>
                        @if($customerId == 6640)
                            <iframe width="100%" height="780"
                                    src="https://datastudio.google.com/embed/reporting/8e4aa70a-6962-4d8c-b613-c35bc746a650/page/LUYgC"
                                    frameborder="0" style="border:0" allowfullscreen>
                            </iframe>
                        @elseif($customerId == 4903)
                            <iframe width="100%" height="780"
                                    src="https://datastudio.google.com/embed/reporting/42d67e29-368a-419b-8d5a-35f4d565ccaa/page/LUYgC"
                                    frameborder="0" style="border:0" allowfullscreen>
                            </iframe>
                        @elseif($customerId == 5000)
                            <iframe width="100%" height="780"
                                    src="https://datastudio.google.com/embed/reporting/c262fe38-c61c-46f4-8500-f0c63605cf9d/page/LUYgC"
                                    frameborder="0" style="border:0" allowfullscreen>
                            </iframe>
                        @elseif($customerId == 4879)
                            <iframe width="100%" height="780"
                                    src="https://datastudio.google.com/embed/reporting/4de58c2a-ba3a-4595-a6e5-e6fbe14e3aa7/page/LUYgC"
                                    frameborder="0" style="border:0" allowfullscreen>
                            </iframe>
                        @elseif($customerId == 4913)
                            <iframe width="100%" height="780"
                                    src="https://datastudio.google.com/embed/reporting/42d67e29-368a-419b-8d5a-35f4d565ccaa/page/LUYgC"
                                    frameborder="0" style="border:0" allowfullscreen>
                            </iframe>
                        @elseif($customerId == 6351)
                            <iframe width="100%" height="780"
                                    src="https://datastudio.google.com/embed/reporting/7033bc40-5f98-4cdb-9c32-2ae595b88a07/page/LUYgC"
                                    frameborder="0" style="border:0" allowfullscreen>
                            </iframe>
                        @elseif($customerId == 6025)
                            <iframe width="100%" height="780"
                                    src="https://datastudio.google.com/embed/reporting/789bee37-4e6b-40af-859c-1f8aa114f2d7/page/LUYgC"
                                    frameborder="0" style="border:0" allowfullscreen>
                            </iframe>
                        @elseif($customerId == 8306)
                            <iframe width="100%" height="780"
                                    src="https://datastudio.google.com/embed/reporting/5d5636a9-0c61-4cd6-b2cd-014837c06ad9/page/LUYgC"
                                    frameborder="0" style="border:0" allowfullscreen>
                            </iframe>
                        @elseif($customerId == 5667)
                            <iframe width="100%" height="780"
                                    src="https://datastudio.google.com/embed/reporting/553bf1e1-1ea6-4b4e-bd30-3edf1b61e5fe/page/LUYgC"
                                    frameborder="0" style="border:0" allowfullscreen>
                            </iframe>
                        @elseif($customerId == 5918)
                            <iframe width="100%" height="780"
                                    src="https://datastudio.google.com/embed/reporting/3522dc29-9662-4197-a515-2d1326b9043e/page/LUYgC"
                                    frameborder="0" style="border:0" allowfullscreen>
                            </iframe>
                        @elseif($customerId == 5922)
                            <iframe width="100%" height="780"
                                    src="https://lookerstudio.google.com/embed/reporting/007e12e6-f80f-4ca9-b283-dd07cc94b662/page/LUYgC"
                                    frameborder="0" style="border:0" allowfullscreen>
                            </iframe>
                            <hr>
                            <iframe width="100%" height="780"
                                    src="https://lookerstudio.google.com/embed/reporting/5649bd34-5ca3-4ca0-8ab4-eb2a220eb402/page/0A0fC"
                                    frameborder="0" style="border:0" allowfullscreen>
                            </iframe>
                        @elseif($customerId == 5927)
                            <iframe width="100%" height="780"
                                    src="https://lookerstudio.google.com/embed/reporting/63b58c5c-a304-4532-84f8-0915d176a823/page/LUYgC"
                                    frameborder="0" style="border:0" allowfullscreen>
                            </iframe>
                            <hr>
                            <iframe width="100%" height="780"
                                    src="https://lookerstudio.google.com/embed/reporting/9c69177b-b25d-4845-8e43-0bb9e6a0c8e9/page/0A0fC"
                                    frameborder="0" style="border:0" allowfullscreen>

                            </iframe>
                        @elseif($customerId == 5930)
                            <iframe width="100%" height="780"
                                    src="https://lookerstudio.google.com/embed/reporting/f6931a5f-5fbd-4493-b915-5ae444f52f75/page/LUYgC"
                                    frameborder="0" style="border:0" allowfullscreen>
                            </iframe>
                            <hr>
                            <iframe width="100%" height="780"
                                    src="https://lookerstudio.google.com/embed/reporting/5cfdab9d-50e2-4d51-96bb-7ccf4ee38385/page/0A0fC"
                                    frameborder="0" style="border:0" allowfullscreen>
                            </iframe>
                        @elseif($customerId == 5931)
                            <iframe width="100%" height="780"
                                    src="https://lookerstudio.google.com/embed/reporting/bd9857ef-f99f-41d2-93e4-94476c33a161/page/LUYgC"
                                    frameborder="0" style="border:0" allowfullscreen>
                            </iframe>
                            <hr>
                            <iframe width="100%" height="780"
                                    src="https://lookerstudio.google.com/embed/reporting/045967e2-b698-473b-b93f-40f9b2400223/page/0A0fC"
                                    frameborder="0" style="border:0" allowfullscreen>
                            </iframe>
                        @elseif($customerId == 4272)
                            <iframe width="100%" height="780"
                                    src="https://datastudio.google.com/embed/reporting/c5492755-cf15-4c1f-980f-169523a50ce4/page/LUYgC"
                                    frameborder="0" style="border:0" allowfullscreen>
                            </iframe>
                        @elseif($customerId == 51234)
                            <iframe width="100%" height="780"
                                    src="https://lookerstudio.google.com/embed/reporting/e9d55d91-48fa-4327-9d4e-23de5d80bc4d/page/LUYgC"
                                    frameborder="0" style="border:0" allowfullscreen>
                            </iframe>
                        @elseif($customerId == 4844)
                            <iframe width="100%" height="780"
                                    src="https://datastudio.google.com/embed/reporting/8ae828f6-1d59-4732-a3e3-34084171d9aa/page/LUYgC"
                                    frameborder="0" style="border:0" allowfullscreen>
                            </iframe>
                        @elseif($customerId == 16941)
                            <iframe width="100%" height="780"
                                    src="https://datastudio.google.com/embed/reporting/16703463-25c2-4c4b-8695-64fa9366972a/page/LUYgC"
                                    frameborder="0" style="border:0" allowfullscreen>
                            </iframe>
                        @elseif($customerId == 8248)
                            <iframe width="100%" height="780"
                                    src="https://datastudio.google.com/embed/reporting/e7acac70-0da1-43ce-9fd1-616741f57614/page/LUYgC"
                                    frameborder="0" style="border:0" allowfullscreen>
                            </iframe>
                        @elseif($customerId == 8252)
                            <iframe width="100%" height="780"
                                    src="https://datastudio.google.com/embed/reporting/7e7442f6-e988-476a-b961-ae8c2eac52ef/page/LUYgC"
                                    frameborder="0" style="border:0" allowfullscreen>
                            </iframe>
                        @elseif($customerId == 8253)
                            <iframe width="100%" height="780"
                                    src="https://datastudio.google.com/embed/reporting/74dc6b62-3c9a-4748-b10d-6c13748bf02b/page/LUYgC"
                                    frameborder="0" style="border:0" allowfullscreen>
                            </iframe>
                        @elseif($customerId == 48221)
                            <iframe width="100%" height="780"
                                    src="https://lookerstudio.google.com/embed/reporting/57afbabc-52c5-4e5b-b395-675c794603d8/page/LUYgC"
                                    frameborder="0" style="border:0" allowfullscreen>
                            </iframe>
                        @elseif($customerId == 47721)
                            <iframe width="100%" height="780"
                                    src="https://lookerstudio.google.com/embed/reporting/517306e7-8a0a-4d5d-95ec-5b8bfcfa2fbe/page/LUYgC"
                                    frameborder="0" style="border:0" allowfullscreen>
                            </iframe>
                        @elseif($customerId == 5926)
                            <iframe width="100%" height="780"
                                    src="https://lookerstudio.google.com/embed/reporting/6c790285-a883-4fa8-8479-266adf762384/page/LUYgC"
                                    frameborder="0" style="border:0" allowfullscreen>
                            </iframe>
                        @elseif($customerId == 44434)
                            <iframe width="100%" height="780"
                                    src="https://lookerstudio.google.com/embed/reporting/d875928c-3826-4412-8e81-c1721e016afe/page/p_pu0yf1btpc"
                                    frameborder="0" style="border:0" allowfullscreen></iframe>
                        @else
                        <div class="mt-4" id="charts"></div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
    </div>
@endsection
