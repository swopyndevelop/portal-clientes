@extends('layouts.app2')
@section('main-content')
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
    <div class="container-fluid">
        <div class="header-body">
        @if(session()->has('message'))
          <div class="alert alert-success alert-dismissible fade show" role="alert">
              <span class="alert-icon"><i class="ni ni-like-2"></i></span>
              <span class="alert-text"><strong>Aviso: </strong> {{ session()->get('message') }}</span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
        @endif
        @if(session()->has('error'))
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
              <span class="alert-icon"><i class="ni ni-like-2"></i></span>
              <span class="alert-text"><strong>Error: </strong> {{ session()->get('error') }}</span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
        @endif
        </div>
    </div>
</div>
 <!-- Page content -->
 <div class="container-fluid mt--8">

      <div class="row mt-5">
        <div class="col">
          <div class="card shadow">
            <div class="card-header bg-transparent border-0">
              <h3 class="text-black mb-0">Mis Áreas Inspecciones</h3>
            </div>
            <div class="table-responsive">
                <div class="col-md-12 table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr class="text-center">
                            <th># Área</th>
                            <th># Orden Servicio</th>
                            <th>Fecha / Hora de Inspección</th>
                            <th>Sucursal</th>
                            <th>Domicilio</th>
                            <th># Áreas Inspeccionadas</th>
                            <th>Opciones</th>
                        </tr>
                        <tr class="text-center">
                            <th>
                                <button id="btnFilterArea" name="btnFilterArea" class="btn btn-default btn-sm">
                                    <i class="ni ni-world-2"></i> Filtrar
                                </button>
                            </th>
                            <th>
                                <input id="areaInspectionFilterArea" name="areaInspectionFilterArea" type="text" class="form-control" style="font-size: .6rem; width: 120px">
                            </th>
                            <th>
                                <input id="dateFilterArea" name="dateFilterArea" type="text" class="form-control" style="font-size: .6rem; width: 120px">
                            </th>
                            <th>
                                <select id="selectCustomerArea" name="selectCustomerArea" class="form-control">
                                    <option value="{{$customer->id}}">{{$customer->name}}</option>
                                    @foreach($customerBranches as $branch)
                                        <option value="{{ $branch->id }}" {{ $branch->id == $selectCustomerBranch ? 'selected' : '' }}>
                                            {{ $branch->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </th>
                            <th>
                                <select id="selectCustomerAddressArea" name="selectCustomerAddressArea" class="form-control">
                                    <option value="0" selected>Todos</option>
                                    @foreach($addressCustomers as $addressCustomer)
                                        <option value="{{ $addressCustomer->id }}">
                                            {{ $addressCustomer->address }}, {{ $addressCustomer->address_number }} {{ $addressCustomer->colony }}
                                            {{ $addressCustomer->municipality }} {{ $addressCustomer->state }}
                                        </option>
                                    @endforeach
                                </select>
                            </th>
                            <th>

                            </th>
                            <th>Opciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $a = 0; ?>
                        @foreach($inspections as $inspection)
                            <?php $a = $inspection->id; ?>
                            <tr>
                                <td>{{ $inspection->id_area }}</td>
                                <td>{{ $inspection->id_service_order }}</td>
                                <td>{{ $inspection->date_inspection }}<br>{{ $inspection->hour_inspection }}</td>
                                <td>{{ $inspection->customer }}<br>{{ $inspection->establishment_name }}</td>
                                <td>{{ $inspection->address }} #{{ $inspection->address_number }},<br>{{ $inspection->colony }},<br>{{ $inspection->municipality }}, {{ $inspection->state }}</td>
                                <td>
                                    {{ $inspection->count_areas }}
                                </td>
                                <td class="text-center">
                                    {{--@include('areas._modalDetailInspection')
                                    <a data-toggle="modal" data-target="#showAreaInspectionModal" class="btn btn-sm btn-primary mb-2" style="color: white">Ver Inspección</a><br>--}}
                                    <a href="{{ route('area_inspection_pdf',  $inspection->id_encoded) }}" target="_blank" class="btn btn-sm btn-success">Descargar Reporte</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
          <br>
          <div class="col-md-12 pagination justify-content-center">
              {{ $inspections->links() }}
          </div>
          </div>
        </div>
      </div>
    </div>
@endsection


