@extends('layouts.app2')

@section('main-content')
    <div class="header bg-gradient-primary pb-6 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">
            </div>
        </div>
    </div>
    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <h3 class="mb-0">Gráficas de Tendencia</h3>
                        <div class="mt-4" id="example"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
