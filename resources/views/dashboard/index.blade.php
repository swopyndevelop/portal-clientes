@extends('layouts.app2')
@section('main-content')
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
    <div class="container-fluid">
        <div class="header-body">
        @if(session()->has('message'))
          <div class="alert alert-success alert-dismissible fade show" role="alert">
              <span class="alert-icon"><i class="ni ni-like-2"></i></span>
              <span class="alert-text"><strong>Aviso: </strong> {{ session()->get('message') }}</span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
        @endif
        @if(session()->has('error'))
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
              <span class="alert-icon"><i class="ni ni-like-2"></i></span>
              <span class="alert-text"><strong>Error: </strong> {{ session()->get('error') }}</span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
        @endif
        </div>
    </div>
</div>
 <!-- Page content -->
 <div class="container-fluid mt--7">
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header border-0">
              <h3 class="mb-0">Mi Agenda</h3>
                <div class="row">
                    <div class="col">
                        <label for="selectCustomerBranch">Filtrar por sucursal</label>
                        <select id="selectCustomerBranch" name="selectCustomerBranch" class="form-control" style="width: 100%;">
                            <option value="">
                                Principal
                            </option>
                            @foreach($customerBranches as $branch)
                                <option value="{{ $branch->id }}" {{ $branch->id == $selectCustomerBranch ? 'selected' : '' }}>
                                    {{ $branch->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col">
                        <button id="btnMain"
                                name="btnMain"
                                style="cursor: pointer"
                                class="btn btn-sm btn-info">
                            <i class="fas fa-undo-alt"></i>
                            Principal
                        </button>
                    </div>
                </div>
            </div>
              <div id="calendar" ></div>
          </div>
        </div>
      </div>
      <!-- Dark table -->
      <div class="row mt-5">
        <div class="col">
          <div class="card shadow">
            <div class="card-header bg-transparent border-0">
              <h3 class="text-black mb-0">Mis Servicios Programados</h3>
            </div>
            <div class="table-responsive">
              <table class="table align-items-center table-flush table-hover">
                <thead class="thead-light">
                  <tr class="text-center">
                    <th>#Clave</th>
                    <th>Sucursal</th>
                    <th>Fecha/Hora <br>de Servicio</th>
                    <th>Técnico</th>
                    <th>Plaga</th>
                    <th>Monto</th>
                    <th>Estatus</th>
                    <th>Opciones</th>
                  </tr>
                  <tr class="text-center">
                      <th>
                          <button id="btnFilterService" name="btnFilterService" class="btn btn-default btn-sm">
                              <i class="ni ni-world-2"></i> Filtrar
                          </button>
                      </th>
                      <th>
                         {{-- <select id="selectBranchService" name="selectBranchService" class="form-control">
                              <option value="0" selected>Todos</option>
                          </select>--}}
                      </th>
                      <th>
                          <input id="dateFilterService" name="dateFilterService" type="text" class="form-control" style="font-size: .6rem; width: 120px">
                      </th>
                      <th>
                          <select id="selectTechnicianService" name="selectTechnicianService" class="form-control">
                              <option value="0" selected>Todos</option>
                              @foreach($technicians as $technician)
                              <option value="{{ $technician->employee_id }}">
                                  {{ $technician->name }}
                              </option>
                              @endforeach
                          </select>
                      </th>
                      <th>
                          <select id="selectPlagueService" name="selectPlagueService" class="form-control">
                              <option value="0" selected>Todos</option>
                              @foreach($plagues as $plague)
                                  <option value="{{ $plague->id }}">
                                      {{ $plague->key }}
                                  </option>
                              @endforeach
                          </select>
                      </th>
                      <th>
                        {{--  <select id="selectMountService" name="selectMountService" class="form-control">
                              <option value="0" selected>Todos</option>
                              <option value="2">Pagado</option>
                              <option value="1">Adeudo</option>
                              <option value="3">Crédito</option>
                              <option value="4">Sin Garantía</option>
                          </select>--}}
                      </th>
                      <th>
                          <select id="selectStatusService" name="selectStatusService" class="form-control">
                              <option value="0" selected>Todos</option>
                              <option value="1">Programado</option>
                              <option value="4">Finalizado</option>
                              <option value="2">Comenzado</option>
                              <option value="3">Cancelado</option>
                              <option value="5">No Realizado</option>
                              <option value="6">Orden de Servicio</option>
                              <option value="7">Garantía</option>
                              <option value="8">Refuerzo</option>
                              <option value="9">Seguimiento</option>
                          </select>
                      </th>
                      <th></th>
                  </tr>
                </thead>
                <tbody class="text-center">
                  @foreach ($order as $o)
                  <tr>
                      <td>
                          {{ $o->id_service_order}}
                      </td>
                      <td>
                          {{ $o->empresa }}
                          <br>
                          {{ $o->municipality }}
                      </td>
                      @if($o->status_order == 10)
                          <td></td>
                        @else
                          <td>
                              {{ \Carbon\Carbon::parse($o->initial_date)->format('d/m/Y') }}
                              <br>
                              {{ \Carbon\Carbon::parse($o->initial_hour)->format('H:i') }}
                          </td>
                    @endif
                    @if($o->status_order == 10)
                          <td></td>
                            @else
                                <td>{{$o->name}}</td>
                            @endif
                    <td>
                        {{$o->key}}
                    </td>
                    <td>
                        @if($o->id_status == 4)
                            @if($o->adeudo == 1)
                                @if($o->show_price == 1 || $o->show_price_customer == 1)
                                    @if($o->show_price_customer == 1 && $o->show_price == 0 || $o->show_price_customer == 1 && $o->show_price == 1)
                                        <strong style="color: #b71c1c">${{$o->total}}</strong>
                                    @endif
                                @endif
                                @if($o->credito == 1)
                                    <br>
                                    <span class="badge badge-pill badge-warning">Crédito</span>
                                @else
                                    <br>
                                    <span class="badge badge-pill badge-danger">Adeudo</span>
                                @endif
                            @else
                                @if($o->adeudo == 2)
                                    <span class="badge badge-pill badge-success">Pagado</span><br>
                                    @if($o->show_price == 1 || $o->show_price_customer == 1)
                                        @if($o->show_price_customer == 1 && $o->show_price == 0 || $o->show_price_customer == 1 && $o->show_price == 1)
                                            <strong>${{$o->total}} Pagado</strong>
                                        @endif
                                    @endif

                                @else
                                    @if($o->show_price == 1 || $o->show_price_customer == 1)
                                        @if($o->show_price_customer == 1 && $o->show_price == 0 || $o->show_price_customer == 1 && $o->show_price == 1)
                                            <strong>${{$o->total}} No</strong>
                                        @endif
                                    @endif
                                @endif
                            @endif
                        @else

                            @if($o->id_status == 1 && $o->adeudo == 2)
                                <span class="badge badge-pill badge-success">Pagado</span><br>
                            @endif
                            @if($o->total == 0 && $o->warranty != 0)
                                    <span class="badge badge-pill badge-warning">Garantía</span>
                                @else
                                    @if($o->show_price == 1 || $o->show_price_customer == 1)
                                        @if($o->show_price_customer == 1 && $o->show_price == 0 || $o->show_price_customer == 1 && $o->show_price == 1)
                                                <strong>${{$o->total}} </strong>
                                        @endif
                                    @endif
                                @endif
                        @endif
                    </td>
                    <td>
                        @if($o->id_status == 1)
                            <span class="badge badge-pill badge-primary">Programado</span>
                            @if($o->confirmed == 1)
                                <br>
                                <strong style="color: #00a157; font-style: bold;">Confirmado</strong>
                            @endif
                        @elseif($o->id_status == 2)
                            <span class="badge badge-pill badge-warning">Comenzado</span>
                        @elseif($o->id_status == 3)
                            <span class="badge badge-pill badge-danger" title="{{ $o->motive }}">Cancelado</span>
                        @elseif($o->id_status == 4)
                            <span class="badge badge-pill badge-success">Finalizado</span>
                        @else
                            <span class="badge badge-pill badge-default">No Programado</span>
                        @endif
                    </td>
                        @if($o->id_status == 4)
                          <td>
                              <a href="{{Route('service',$o->order)}}" target="_blank" class="btn btn-sm btn-primary">Ver Servicio</a><br><br>
                              <a href="{{Route('service_order',$o->order)}}" target="_blank" class="btn btn-sm btn-success" data-toggle="tooltip" title="Orden de Servicio">
                                  <i class="ni ni-folder-17"></i>
                              </a>
                              <a href="{{Route('download',$o->order)}}" target="_blank" class="btn btn-sm btn-success" data-toggle="tooltip" title="Certificado de Servicio">
                                  <i class="ni ni-folder-17"></i>
                              </a>
                        </td>
                        @else
                        <td></td>
                        @endif
                      @endforeach
                  </tr>
                </tbody>
              </table>
            </div>
          <br>
          <div class="col-md-12 pagination justify-content-center">
              {{ $order->appends(request()->query())->links() }}
          </div>
          </div>
        </div>
      </div>
    </div>
<br><br><br><br><br>
@endsection
<script>
	document.addEventListener('DOMContentLoaded', function() {

let events = [
  @foreach ($events as $event)
    {
      @if($event->id_status == 4) title:'{{ $event -> title }} \n (FINALIZADO)' @else title:'{{ $event -> title }}' @endif,
      id: '{{ $event -> id }}',
      start: '{{ $event -> initial_date }}' + ' ' + '{{ $event -> initial_hour }}',
      end: '{{ $event -> final_date }}' + ' ' + '{{ $event -> final_hour }}',
      @if($event->id_status == 4) color: '#00a157' @else color: '{{ $event -> color }}' @endif,
    },
  @endforeach
];

let min = '08:00:00';
let max = '20:00:00';
let modeFull = false;

let calendarEl = document.getElementById('calendar');

let calendar = new FullCalendar.Calendar(calendarEl, {
  plugins: [ 'interaction', 'dayGrid', 'timeGrid', 'momentTimezonePlugin'],
  timeZone: 'local',
  defaultView: 'timeGridWeek',
  locale: 'es',
  customButtons: {
    changeRange: {
      text: '12/24',
      click: function () {
        if (modeFull) {
          calendar.setOption('minTime', '08:00:00');
          calendar.setOption('maxTime', '20:00:00');
          modeFull = false;
        }else {
          calendar.setOption('minTime', '00:00:00');
          calendar.setOption('maxTime', '24:00:00');
          modeFull = true;
        }
      }
    }
  },
  header: {
    left: 'prev,next,changeRange',
    center: 'title',
    right: 'dayGridMonth,timeGridWeek,timeGridDay'
  },
  minTime: min,
  maxTime: max,
  contentHeight: "auto",
  events: events,
  eventClick: function(info) {
    //getDataEvent(info.event.id);
  },
  selectable: true,
    selectMirror: true,
    select: function(arg) {

      var initialHour = moment(arg.start).format('HH:mm');
        var finalHour = moment(arg.end).format('HH:mm');
        var initialDate = moment(arg.start).format('YYYY-MM-DD');
        var finalDate = moment(arg.end).format('YYYY-MM-DD');
        var title ='Fumigación...';

        //set values event
        document.getElementById("dateStart").value = initialDate;
        document.getElementById("dateEnd").value = finalDate;
        document.getElementById("hourStart").value = initialHour;
        document.getElementById("hourEnd").value = finalHour;

        //show modal

        /*if (isEvent) {
            calendar.addEvent({
            title: title,
            start: arg.start,
            end: arg.end,
            allDay: arg.allDay
          })
        }*/

      calendar.unselect()
    },
  editable: true,
});

calendar.render();
});

//get price from database
function getDataEvent(id){
  loader('');
    $.ajax({
        type: 'GET',
        url: route('info_event', id),
        data: {
            _token: $("meta[name=csrf-token]").attr("content")
        },
        success: function (data) {
            if (data.errors){
                Swal.close();
                missingText('Algo salio mal');
            }
            else{
                var json = JSON.parse(JSON.stringify(data));
                Swal.close();
    Swal.fire({
      type: 'info',
      showCancelButton: true,
        confirmButtonColor: '#018243',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Editar',
        cancelButtonText: 'Cerrar',
      title: '<strong style="font-size:20px; color: #018243;">' + data.name + '&nbsp;&nbsp;&nbsp;&nbsp;' + data.id_service_order + '</strong>',
      html: '<hr><strong style="font-size:15px; color: #1F282C;">' + data.initial_date + '&nbsp;&nbsp;&nbsp;&nbsp;' + data.initial_hour +'</strong><br><br>' +
            '<strong style="font-size:15px; color: #1F282C;">' + data.plagues + '</strong><br><br>' +
          '<strong style="font-size:15px; color: #1F282C;">Total: $' + data.total + '<strong><br><br>' +
          '<strong style="font-size:15px; color: #1F282C;">Teléfono: ' + data.cellphone + '<strong><br><br>' +
          '<strong style="font-size:15px; color: #1F282C;">' + data.address + '<strong><br><br>' +
          '<strong style="font-size:15px; color: #1F282C;">Técnico Asignado: ' + data.technical + '<strong><hr>',
      width: 800,
    }).then((result) => {
      if (result.value) {
        Swal.close();
        //edit order service.
        window.location = route('edit_event', id);
      }
    });
            }
        }
    });
}

function loader(message){
  Swal.fire({
      title: message,
allowOutsideClick: false,
      onBeforeOpen: () => {
          Swal.showLoading()
      },
  }).then((result) => {

  })
}

function missingText(textError) {
  swal({
      title: "¡Espera!",
      type: "error",
      text: textError,
      icon: "error",
      timer: 3000,
      showCancelButton: false,
      showConfirmButton: false
  });
}
</script>

