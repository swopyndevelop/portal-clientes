@extends('layouts.app2')
@section('main-content')
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
    <div class="container-fluid">
        <div class="header-body">
        @if(session()->has('message'))
          <div class="alert alert-success alert-dismissible fade show" role="alert">
              <span class="alert-icon"><i class="ni ni-like-2"></i></span>
              <span class="alert-text"><strong>Aviso: </strong> {{ session()->get('message') }}</span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
        @endif
        @if(session()->has('error'))
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
              <span class="alert-icon"><i class="ni ni-like-2"></i></span>
              <span class="alert-text"><strong>Error: </strong> {{ session()->get('error') }}</span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
        @endif
        </div>
    </div>
</div>
 <!-- Page content -->
 <div class="container-fluid mt--8">

      <div class="row mt-5">
        <div class="col">
          <div class="card shadow">
            <div class="card-header bg-transparent border-0">
              <h3 class="text-black mb-0">Mis Inspecciones</h3>
            </div>
            <div class="table-responsive">
              <table class="table align-items-center table-flush table-hover">
                <thead class="thead-light">
                  <tr>
                      <th># Monitoreo</th>
                      <th># Inspección</th>
                      <th>Fecha / Hora de Inspección</th>
                      <th>Sucursal</th>
                      <th>Domicilio</th>
                      <th># Estaciones</th>
                      <th>Opciones</th>
                  </tr>
                  <tr>
                      <th>
                          <button id="btnFilterMonitoring" name="btnFilterMonitoring" class="btn btn-default btn-sm">
                              <i class="ni ni-world-2"></i> Filtrar
                          </button>
                      </th>
                      <th>
                          <input id="inspectionFilterMonitoring" name="inspectionFilterMonitoring" type="text" class="form-control" style="font-size: .6rem; width: 120px">
                      </th>
                      <th>
                          <input id="dateFilterMonitoring" name="dateFilterMonitoring" type="text" class="form-control" style="font-size: .6rem; width: 120px">
                      </th>
                      <th>
                          <select id="selectCustomerMonitoring" name="selectCustomerMonitoring" class="form-control">
                              <option value="{{$customer->id}}">{{$customer->name}}</option>
                              @foreach($customerBranches as $branch)
                                  <option value="{{ $branch->id }}" {{ $branch->id == $selectCustomerBranch ? 'selected' : '' }}>
                                      {{ $branch->name }}
                                  </option>
                              @endforeach
                          </select>
                      </th>
                      <th>
                          <select id="selectCustomerAddressMonitoring" name="selectCustomerAddressMonitoring" class="form-control">
                              <option value="0" selected>Todos</option>
                              @foreach($addressCustomers as $addressCustomer)
                                  <option value="{{ $addressCustomer->id }}">
                                      {{ $addressCustomer->address }}, {{ $addressCustomer->address_number }} {{ $addressCustomer->colony }}
                                      {{ $addressCustomer->municipality }} {{ $addressCustomer->state }}
                                  </option>
                              @endforeach
                          </select>
                      </th>
                      <th>

                      </th>
                      <th>Opciones</th>
                  </tr>
                </thead>
                <tbody>
                <?php $a = 0; ?>
                @foreach($inspections as $inspection)
                    <?php $a = $inspection->id; ?>
                    <tr>
                        <td>{{ $inspection->id_monitoring }}</td>
                        <td>{{ $inspection->id_inspection }}</td>
                        <td>{{ $inspection->date }}<br>{{ $inspection->hour }}<br>{{ $inspection->technician }}</td>
                        <td>{{ $inspection->customer }}<br>{{ $inspection->establishment_name }}</td>
                        <td>{{ $inspection->address }} #{{ $inspection->address_number }},<br>{{ $inspection->colony }},<br>{{ $inspection->municipality }}, {{ $inspection->state }}</td>
                        <td>
                            @foreach($inspection->nodes as $node)
                                {{ $node->count }} {{ $node->name }}<br>
                            @endforeach
                        </td>
                        <td class="text-center">
                            @include('inspection.modalInspectionDetail')
                          <a data-toggle="modal" data-target="#inspectionDetail<?php echo $a; ?>" class="btn btn-sm btn-primary mb-2" style="color: white">Ver Inspección</a><br>
                          <a href="{{ route('station_monitoring_pdf', $inspection->id_service_order) }}" target="_blank" class="btn btn-sm btn-success">Descargar Reporte</a>
                        </td>
                  </tr>
                @endforeach
                </tbody>
              </table>
            </div>
          <br>
          <div class="col-md-12 pagination justify-content-center">
              {{ $inspections->links() }}
          </div>
          </div>
        </div>
      </div>
    </div>
@endsection


