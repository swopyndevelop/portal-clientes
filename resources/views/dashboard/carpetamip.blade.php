@extends('layouts.app2')
@section('main-content')
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
    <div class="container-fluid">
        <div class="header-body">
            <input type="hidden" value="{{ $customerId }}" id="customerId">
        @if(session()->has('message'))
          <div class="alert alert-success alert-dismissible fade show" role="alert">
              <span class="alert-icon"><i class="ni ni-like-2"></i></span>
              <span class="alert-text"><strong>Aviso: </strong> {{ session()->get('message') }}</span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
        @endif
        @if(session()->has('error'))
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
              <span class="alert-icon"><i class="ni ni-like-2"></i></span>
              <span class="alert-text"><strong>Error: </strong> {{ session()->get('error') }}</span>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
        @endif
        </div>
    </div>
</div>
 <!-- Page content -->
 <div class="container-fluid mt--7">
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card shadow">
            <div class="card-header bg-transparent border-0">
              <h3 class="text-black mb-0">Carpeta MIP</h3>
                <div class="row">
                    <div class="col">
                        <label for="selectCustomerBranchMip">Filtrar por sucursal</label>
                        <select id="selectCustomerBranchMip" name="selectCustomerBranchMip" class="form-control">
                            <option value="{{$customer->id}}">{{$customer->name}}</option>
                            @foreach($customerBranches as $branch)
                                <option value="{{ $branch->id }}">
                                    {{ $branch->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col">
                        <label>Descargar Carpeta Mip</label> <br>
                        <button id="BtnMipComplete"
                                class="btn btn-sm btn-block btn-success">Descargar Mip
                        </button>
                    </div>
                    <div class="col">
                        <label>Carpeta Mip Archivo</label> <br>
                        <button id="BtnMipCompleteOnly"
                                class="btn btn-sm btn-block btn-success">Descargar Mip Personalizado
                        </button>
                    </div>
                </div>

                <br>
                <div class="row">
                  <div class="col">
                      <select id="selectYearOrders" class="form-control"></select>
                  </div>
                  <div class="col">
                      <select id="selectMonthOrders" class="form-control"></select>
                  </div>
              </div>
                <hr>
            <div class="table-responsive">
              <table class="table align-items-center table-flush">
                <thead class="thead-light">
                  <tr>
                   {{-- <th scope="col">Año</th>
                    <th scope="col">Opciones</th>--}}
                  </tr>
                </thead>
                <tbody id="table-body-mips">
                @if($customerId == 6640)

                    <tr>
                        <td>Licencia Sanitaria</td>
                        <td>
                            <select class="form-control" id="selectLicense">
                                <option value="2021" selected>2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                            </select>
                        </td>
                        <td>
                            <button id="btnLicense"
                                    style="cursor: pointer"
                                    class="btn btn-sm btn-success">Descargar
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td>Análisis de Riesgo</td>
                        <td>
                            <select class="form-control" id="selectAnalisis">
                                <option value="2021" selected>2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                            </select>
                        </td>
                        <td>
                            <button id="btnAnalisis"
                                    style="cursor: pointer"
                                    class="btn btn-sm btn-success">Descargar
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td>Poliza de Seguro</td>
                        <td>
                            <select class="form-control" id="selectPolicy">
                                <option value="2021" selected>2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                            </select>
                        </td>
                        <td>
                            <button id="btnPolicy"
                                    style="cursor: pointer"
                                    class="btn btn-sm btn-success">Descargar
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td>Plan De Trabajo</td>
                        <td>
                            <select class="form-control" id="selectPlan">
                                <option value="2021" selected>2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                            </select>
                        </td>
                        <td>
                            <button id="btnPlan"
                                    style="cursor: pointer"
                                    class="btn btn-sm btn-success">Descargar
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td>Procedimientos Interior y Exterior</td>
                        <td>
                            <select class="form-control" id="selectProcess">
                                <option value="2021" selected>2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                            </select>
                        </td>
                        <td>
                            <button id="btnProcess"
                                    style="cursor: pointer"
                                    class="btn btn-sm btn-success">Descargar
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td>Normas Mexicanas</td>
                        <td>
                            <select class="form-control" id="selectNormas">
                                <option value="2021" selected>2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                            </select>
                        </td>
                        <td>
                            <button id="btnNormas"
                                    style="cursor: pointer"
                                    class="btn btn-sm btn-success">Descargar
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td>Capacitación</td>
                        <td>
                            <select class="form-control" id="selectCapacitation">
                                <option value="2023" selected>2023</option>
                            </select>
                        </td>
                        <td>
                            <button id="btnCapacitation"
                                    style="cursor: pointer"
                                    class="btn btn-sm btn-success">Descargar
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td>Información Plaguicidas</td>
                        <td>
                            <select class="form-control" id="selectProducts">
                                <option value="2021">2021</option>
                                <option value="2022" selected>2022</option>
                                <option value="2023">2023</option>
                            </select>
                        </td>
                        <td>
                            <button id="btnProducts"
                                    style="cursor: pointer"
                                    class="btn btn-sm btn-success">Descargar
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td>Calendario de Visitas</td>
                        <td>
                            <select class="form-control" id="selectCalendar">
                                <option value="2021" selected>2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                            </select>
                        </td>
                        <td>
                            <button id="btnCalendar"
                                    style="cursor: pointer"
                                    class="btn btn-sm btn-success">Descargar
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td>Certificados de Fumigación</td>
                        <td>
                            <select class="form-control" id="selectServices">
                                <option value="2021">2021</option>
                                <option value="2022" selected>2022</option>
                                <option value="2023">2023</option>
                            </select>
                        </td>
                        <td>
                            <button id="btnServices"
                                    style="cursor: pointer"
                                    class="btn btn-sm btn-success">Descargar
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td>Ordenes de Servicio</td>
                        <td>
                            <select class="form-control" id="selectOrders">
                                <option value="2021">2021</option>
                                <option value="2022" selected>2022</option>
                                <option value="2023">2023</option>
                            </select>
                        </td>
                        <td>
                            <button id="btnOrders"
                                    style="cursor: pointer"
                                    class="btn btn-sm btn-success">Descargar
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td>Inspecciones</td>
                        <td>
                            <select class="form-control" id="selectInspections">
                                <option value="2021">2021</option>
                                <option value="2022" selected>2022</option>
                                <option value="2023">2023</option>
                            </select>
                        </td>
                        <td>
                            <button id="btnInspections"
                                    style="cursor: pointer"
                                    class="btn btn-sm btn-success">Descargar
                            </button>
                        </td>
                    </tr>
                @endif
                @if($customerId == 44434)

                   {{-- <tr>
                        <td>2023</td>
                        <td>
                            <a href="https://pestwareapp.com/api/js/portal/customer/mip/2023/44434/2023/" target="_blank" class="btn btn-sm btn-success">Descargar MIP Actualizada</a>
                        </td>
                    </tr>
                    <tr>
                        <td>2024</td>
                        <td>
                            <a href="https://pestwareapp.com/api/js/portal/customer/mip/2024/44434/2024/" target="_blank" class="btn btn-sm btn-success">Descargar MIP Actualizada</a>
                        </td>
                    </tr>--}}
                @endif
                </tbody>
              </table>
            </div>
              <div class="container">
                  <div class="row">
                      <div class="col">
                          <h3 class="text-center">2023</h3>
                          <div class="table-responsive">
                              <table class="table align-items-center table-flush">
                                  <thead class="thead-light">
                                  <tr>
                                      <th scope="col">Folio</th>
                                      <th scope="col">Documentos</th>
                                  </tr>
                                  </thead>
                                  <tbody id="table-body-services-2023">
                                  </tbody>
                              </table>
                          </div>
                      </div>
                      <div class="col">
                          <h3 class="text-center">2024</h3>
                          <div class="table-responsive">
                              <table class="table align-items-center table-flush">
                                  <thead class="thead-light">
                                  <tr>
                                      <th scope="col">Folio</th>
                                      <th scope="col">Documentos</th>
                                  </tr>
                                  </thead>
                                  <tbody id="table-body-services-2024">
                                  </tbody>
                              </table>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
        </div>
      </div>
<br><br><br><br><br>
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

 @if($customerId == 6640)
 <script>
     $( document ).ready(function() {
         // Add Btn Year MIp
         $('#btnLicense').click(function() {
             let selectLicense = $('#selectLicense option:selected').val();
             window.open('https://pestwareapp.com/api/js/portal/customer/mip/license/6640/' + selectLicense, '_blank')
         });
         $('#btnAnalisis').click(function() {
             let selectLicense = $('#selectAnalisis option:selected').val();
             window.open('https://pestwareapp.com/api/js/portal/customer/mip/analisis/6640/' + selectLicense, '_blank')
         });
         $('#btnPolicy').click(function() {
             let selectLicense = $('#selectPolicy option:selected').val();
             window.open('https://pestwareapp.com/api/js/portal/customer/mip/policy/6640/' + selectLicense, '_blank')
         });
         $('#btnPlan').click(function() {
             let selectLicense = $('#selectPlan option:selected').val();
             window.open('https://pestwareapp.com/api/js/portal/customer/mip/plan/6640/' + selectLicense, '_blank')
         });
         $('#btnProcess').click(function() {
             let selectLicense = $('#selectProcess option:selected').val();
             window.open('https://pestwareapp.com/api/js/portal/customer/mip/process/6640/' + selectLicense, '_blank')
         });
         $('#btnNormas').click(function() {
             let selectLicense = $('#selectNormas option:selected').val();
             window.open('https://pestwareapp.com/api/js/portal/customer/mip/normas/6640/' + selectLicense, '_blank')
         });
         $('#btnCapacitation').click(function() {
             let selectLicense = $('#selectCapacitation option:selected').val();
             window.open('https://pestwareapp.com/api/js/portal/customer/mip/capacitation/6640/' + selectLicense, '_blank')
         });
         $('#btnProducts').click(function() {
             let selectLicense = $('#selectProducts option:selected').val();
             window.open('https://pestwareapp.com/api/js/portal/customer/mip/products/6640/' + selectLicense, '_blank')
         });
         $('#btnCalendar').click(function() {
             let selectLicense = $('#selectCalendar option:selected').val();
             window.open('https://pestwareapp.com/api/js/portal/customer/mip/calendar/6640/' + selectLicense, '_blank')
         });
         $('#btnServices').click(function() {
             let selectLicense = $('#selectServices option:selected').val();
             window.open('https://pestwareapp.com/api/js/portal/customer/mip/services/6640/' + selectLicense, '_blank')
         });
         $('#btnOrders').click(function() {
             let selectLicense = $('#selectOrders option:selected').val();
             window.open('https://pestwareapp.com/api/js/portal/customer/mip/orders/6640/' + selectLicense, '_blank')
         });
         $('#btnInspections').click(function() {
             let selectLicense = $('#selectInspections option:selected').val();
             window.open('https://pestwareapp.com/api/js/portal/customer/mip/inspections/6640/' + selectLicense, '_blank')
         });


         // Mip Update Complete
         let selectYear = document.getElementById('selectYearOrders');
         let currentYear = new Date().getFullYear();
         let year = currentYear;
         let startYear = 2021;
         let endYear = 2030;
         let initialDate = 0, finalDate = 0;
         for (let year = startYear; year <= endYear; year++) {
             let option = document.createElement('option');
             option.value = year;
             option.text = year;
             if (year === currentYear) option.selected = true;
             selectYear.appendChild(option);
         }

         let selectMonth = document.getElementById('selectMonthOrders');
         let currentMonth = new Date().getMonth();
         let startMonth = 0;
         let endMonth = 12;
         for (let month = startMonth; month <= endMonth; month++) {
             let option = document.createElement('option');
             option.value = month;
             if (month == 0) option.text = 'Todos';
             if (month == 1) option.text = 'Enero';
             if (month == 2) option.text = 'Febrero';
             if (month == 3) option.text = 'Marzo';
             if (month == 4) option.text = 'Abril';
             if (month == 5) option.text = 'Mayo';
             if (month == 6) option.text = 'Junio';
             if (month == 7) option.text = 'Julio';
             if (month == 8) option.text = 'Agosto';
             if (month == 9) option.text = 'Septiembre';
             if (month == 10) option.text = 'Octubre';
             if (month == 11) option.text = 'Noviembre';
             if (month == 12) option.text = 'Diciembre';

             if (month === 0) option.selected = true;
             selectMonth.appendChild(option);
         }

         $('#BtnMipComplete').click(function() {
             let year = $('#selectYearOrders option:selected').val();
             let month = $('#selectMonthOrders option:selected').val();
             //let route = 'http://127.0.0.1:8000/pdf/download/mip/';
             let route = 'https://pestwareapp.com/pdf/download/mip/';
             let newRoute = route + customerId + '/' + year + '/' + month;
             window.open(newRoute, '_blank')
         });

     });
 </script>
 @else
 <script>
             $( document ).ready(function() {
                 let customerId = document.getElementById("selectCustomerBranchMip").value;

                 // Orders 2023 y 2024
                 $.ajax({
                     type: 'GET',
                     url: `https://pestwareapp.com/api/js/portal/customer/services/${customerId}/2023`,
                     success: function(data) {
                         let rows = '';
                         data.data.forEach((element) => {
                             rows += `<tr>
                      <td>${element.name}</td>
                      <td>
                        <a href="${element.urlService}" target="_blank" style="color: green" >Servicio</a> ::
                        <a href="${element.urlOrder}" target="_blank" style="color: red">Orden</a>
                      </td>
                    </tr>`;
                         });
                         $('#table-body-services-2023').append(rows);
                     }
                 });

                 $.ajax({
                     type: 'GET',
                     url: `https://pestwareapp.com/api/js/portal/customer/services/${customerId}/2024`,
                     success: function(data) {
                         let rows = '';
                         data.data.forEach((element) => {
                             rows += `<tr>
                      <td>${element.name}</td>
                      <td>
                        <a href="${element.urlService}" target="_blank" style="color: green" >Servicio</a> ::
                        <a href="${element.urlOrder}" target="_blank" style="color: red">Orden</a>
                      </td>
                    </tr>`;
                         });
                         $('#table-body-services-2024').append(rows);
                     }
                 });

                 // Mip Update Complete
                 let selectYear = document.getElementById('selectYearOrders');
                 let currentYear = new Date().getFullYear();
                 let year = currentYear;
                 let startYear = 2021;
                 let endYear = 2030;
                 let initialDate = 0, finalDate = 0;
                 for (let year = startYear; year <= endYear; year++) {
                     let option = document.createElement('option');
                     option.value = year;
                     option.text = year;
                     if (year === currentYear) option.selected = true;
                     selectYear.appendChild(option);
                 }

                 let selectMonth = document.getElementById('selectMonthOrders');
                 let currentMonth = new Date().getMonth();
                 let startMonth = 0;
                 let endMonth = 12;
                 for (let month = startMonth; month <= endMonth; month++) {
                     let option = document.createElement('option');
                     option.value = month;
                     if (month == 0) option.text = 'Todos';
                     if (month == 1) option.text = 'Enero';
                     if (month == 2) option.text = 'Febrero';
                     if (month == 3) option.text = 'Marzo';
                     if (month == 4) option.text = 'Abril';
                     if (month == 5) option.text = 'Mayo';
                     if (month == 6) option.text = 'Junio';
                     if (month == 7) option.text = 'Julio';
                     if (month == 8) option.text = 'Agosto';
                     if (month == 9) option.text = 'Septiembre';
                     if (month == 10) option.text = 'Octubre';
                     if (month == 11) option.text = 'Noviembre';
                     if (month == 12) option.text = 'Diciembre';

                     if (month === 0) option.selected = true;
                     selectMonth.appendChild(option);
                 }

                 $('#BtnMipComplete').click(function() {
                     customerId = $('#selectCustomerBranchMip option:selected').val();
                     let year = $('#selectYearOrders option:selected').val();
                     let month = $('#selectMonthOrders option:selected').val();
                     //let route = 'http://127.0.0.1:8000/pdf/download/mip/';
                     let route = 'https://pestwareapp.com/pdf/download/mip/';
                     let newRoute = route + customerId + '/' + year + '/' + month;
                     window.open(newRoute, '_blank')
                 });

                 $('#BtnMipCompleteOnly').click(function() {
                     customerId = $('#selectCustomerBranchMip option:selected').val();
                     //let route = 'http://127.0.0.1:8000/pdf/download/mip/';
                     let route = 'https://pestwareapp.com/pdf/download/mip/';
                     let newRoute = route + customerId;
                     window.open(newRoute, '_blank')
                 });

             });
         </script>
@endif
