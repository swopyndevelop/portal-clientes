@extends('layouts.app2')
@section('main-content')
    <div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
        <div class="container-fluid">
            <div class="header-body">
                @if(session()->has('message'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <span class="alert-icon"><i class="ni ni-like-2"></i></span>
                        <span class="alert-text"><strong>Aviso: </strong> {{ session()->get('message') }}</span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                @if(session()->has('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <span class="alert-icon"><i class="ni ni-bell-55"></i></span>
                        <span class="alert-text"><strong>Error: </strong> {{ session()->get('error') }}</span>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--8">

        <div class="row mt-5">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header bg-transparent border-0">
                        <h3 class="text-black mb-0">Mis Seguimientos</h3>
                    </div>
                    <div class="row container-fluid">
                        <div class="col">
                            <a href="#" data-toggle="modal" data-target="#modalCreateTracing" data-customer="{{$customer->id}}"
                               data-branch="{{$selectCustomerBranch}}" class="btn btn-success btn-fab btn-icon btn- btn-sm">
                                <i class="ni ni-fat-add"></i> Nuevo Seguimiento
                            </a>
                        </div>
                        <div class="col">
                            {{--<label for="selectCustomerBranchTracing">Filtrar por sucursal</label>--}}
                            <select id="selectCustomerBranchTracing" name="selectCustomerBranchTracing" class="form-control" style="width: 100%;">
                                <option value="">
                                    Principal
                                </option>
                                @foreach($customerBranches as $branch)
                                    <option value="{{ $branch->id }}" {{ $branch->id == $selectCustomerBranch ? 'selected' : '' }}>
                                        {{ $branch->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <br>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush table-hover">
                            <thead class="thead-light">
                            <tr>
                                <th># Seguimiento</th>
                                <th>Fecha / Hora de Solicitud</th>
                                <th>Fecha / Hora de Asignación</th>
                                <th>Técnico Asignado</th>
                                <th>Comentarios</th>
                                <th>Evidencia</th>
                                <th>Estatus</th>
                            </tr>
                            <tr class="text-center">
                                <th>
                                    <input id="folioFilterTracing" name="folioFilterTracing" type="text" class="form-control" style="font-size: .6rem; width: 120px">
                                </th>
                                <th>
                                    <input id="dateFilterApplication" name="dateFilterApplication" type="text" class="form-control" style="font-size: .6rem; width: 120px">
                                </th>
                                <th>
                                    <input id="dateFilterAssignment" name="dateFilterAssignment" type="text" class="form-control" style="font-size: .6rem; width: 120px">
                                </th>
                                <th>
                                    <select id="selectTechnicianTracing" name="selectTechnicianTracing" class="form-control">
                                        <option value="0" selected>Todos</option>
                                        @foreach($employees as $employee)
                                            <option value="{{ $employee->id }}">
                                                {{ $employee->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </th>
                                <th>
                                    <input id="folioFilterComment" name="folioFilterComment" type="text" class="form-control" style="font-size: .6rem; width: 120px">
                                </th>
                                <th>
                                    <button id="btnFilterTracing" name="btnFilterTracing" class="btn btn-default btn-sm">
                                        <i class="ni ni-world-2"></i> Filtrar
                                    </button>
                                </th>
                                <th>

                                </th>

                            </thead>
                            <tbody>
                            @foreach($tracings as $tracing)
                                <tr>
                                    <td>
                                        <b>{{ $tracing->id_service_order }}</b>
                                    </td>
                                    <td>
                                        {{ \Carbon\Carbon::parse($tracing->created_at)->format('d/m/Y') }} <br>
                                        {{ \Carbon\Carbon::parse($tracing->created_at)->format('H:i') }}
                                    </td>
                                    <td>
                                        @if($tracing->initial_date)
                                            {{ \Carbon\Carbon::parse($tracing->initial_date)->format('d/m/Y') }} <br>
                                            {{ \Carbon\Carbon::parse($tracing->initial_hour)->format('H:i') }}
                                        @else
                                            <span class="badge badge-pill badge-info">Pendiente por Agendar</span>
                                        @endif
                                    </td>
                                    <td>{{ $tracing->name }}</td>
                                    <td>
                                        @if($tracing->tracing_comments)
                                            {{ $tracing->tracing_comments }}
                                        @else
                                            Sin Comentarios
                                        @endif
                                    </td>
                                    <td>
                                        @if($tracing->evidence_tracings != null)
                                            <div id="selectorEvidence" style="cursor: pointer">
                                                <span class="item" data-src="{{ env('URL_STORAGE_FTP').$tracing->evidence_tracings }}">
                                                    <img src="{{ env('URL_STORAGE_FTP').$tracing->evidence_tracings }}" alt="Evidencia" width="50px" height="50px">
                                                </span>
                                            </div>
                                        @endif
                                    </td>
                                    <td>
                                        @if($tracing->id_status == null)
                                            <span class="badge badge-pill badge-default">Sin Programar</span>
                                        @elseif($tracing->id_status == 1)
                                            <span class="badge badge-pill badge-primary">Programado</span>
                                        @elseif($tracing->id_status == 2)
                                            <span class="badge badge-pill badge-warning">Comenzado</span>
                                        @elseif($tracing->id_status == 3)
                                            <span class="badge badge-pill badge-danger">Cancelado</span>
                                        @elseif($tracing->id_status == 4)
                                            <span class="badge badge-pill badge-success">Finalizado</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <br>
                    <div class="col-md-12 pagination justify-content-center">
                        {{ $tracings->links() }}
                    </div>
                </div>
            </div>
        </div>
        <br><br>
    </div>
    @include('dashboard.newTracingModal')
@endsection


